-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema homecook
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema homecook
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `homecook` DEFAULT CHARACTER SET utf8 ;
USE `homecook` ;

-- -----------------------------------------------------
-- Table `homecook`.`UserRole`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `homecook`.`UserRole` ;

CREATE TABLE IF NOT EXISTS `homecook`.`UserRole` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `role` VARCHAR(45) NOT NULL,
  `description` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `homecook`.`User`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `homecook`.`User` ;

CREATE TABLE IF NOT EXISTS `homecook`.`User` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `userRoleId` INT NOT NULL,
  `username` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `password` VARCHAR(64) NOT NULL,
  `job` VARCHAR(45) NULL,
  `picture` VARCHAR(256) NULL,
  `description` VARCHAR(512) NULL,
  `servings` INT NOT NULL,
  `active` TINYINT(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC),
  INDEX `fk_UserRole_idx` (`userRoleId` ASC),
  CONSTRAINT `fk_User_UserRole`
    FOREIGN KEY (`userRoleId`)
    REFERENCES `homecook`.`UserRole` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `homecook`.`Recipe`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `homecook`.`Recipe` ;

CREATE TABLE IF NOT EXISTS `homecook`.`Recipe` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `userId` INT NULL,
  `name` VARCHAR(128) NOT NULL,
  `description` VARCHAR(2048) NOT NULL,
  `image` VARCHAR(256) NOT NULL,
  `servings` INT NOT NULL,
  `price` DOUBLE NOT NULL,
  `time` TIME NOT NULL,
  `difficulty` ENUM('1', '2', '3', '4', '5') NOT NULL,
  `showDescription` VARCHAR(256) NULL,
  `isBreakfast` TINYINT(1) NULL,
  `access` ENUM('private', 'public', 'restricted') NULL,
  `active` TINYINT(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_User_idx` (`userId` ASC),
  CONSTRAINT `fk_User`
    FOREIGN KEY (`userId`)
    REFERENCES `homecook`.`User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `homecook`.`IngredientCategory`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `homecook`.`IngredientCategory` ;

CREATE TABLE IF NOT EXISTS `homecook`.`IngredientCategory` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `homecook`.`Ingredient`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `homecook`.`Ingredient` ;

CREATE TABLE IF NOT EXISTS `homecook`.`Ingredient` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `ingredientCategoryId` INT NOT NULL,
  `name` VARCHAR(256) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Ingredient_IngredienteCategory_idx` (`ingredientCategoryId` ASC),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  CONSTRAINT `fk_Ingredient_IngredienteCategory`
    FOREIGN KEY (`ingredientCategoryId`)
    REFERENCES `homecook`.`IngredientCategory` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `homecook`.`Unit`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `homecook`.`Unit` ;

CREATE TABLE IF NOT EXISTS `homecook`.`Unit` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `aggregate` TINYINT(1) NOT NULL,
  `abbreviation` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `homecook`.`Meal`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `homecook`.`Meal` ;

CREATE TABLE IF NOT EXISTS `homecook`.`Meal` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `userId` INT NOT NULL,
  `recipeId` INT NOT NULL,
  `date` DATE NOT NULL,
  `mealTime` ENUM('Breakfast', 'Lunch', 'Dinner') NOT NULL,
  `servings` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_UserRecipe_Recipe_idx` (`recipeId` ASC),
  UNIQUE INDEX `Meal_UNIQUE` (`recipeId` ASC, `mealTime` ASC, `userId` ASC, `date` ASC),
  CONSTRAINT `fk_UserRecipe_User`
    FOREIGN KEY (`userId`)
    REFERENCES `homecook`.`User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_UserRecipe_Recipe`
    FOREIGN KEY (`recipeId`)
    REFERENCES `homecook`.`Recipe` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `homecook`.`RecipeIngredient`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `homecook`.`RecipeIngredient` ;

CREATE TABLE IF NOT EXISTS `homecook`.`RecipeIngredient` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `recipeId` INT NOT NULL,
  `ingredientId` INT NOT NULL,
  `unitId` INT NOT NULL,
  `quantity` DECIMAL(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_RecipeIngredient_Recipe_idx` (`recipeId` ASC),
  INDEX `fk_RecipeIngredient_Ingredient_idx` (`ingredientId` ASC),
  INDEX `fk_RecipeIngredient_Unit_idx` (`unitId` ASC),
  UNIQUE INDEX `RecipeIngredient_UNIQUE` (`recipeId` ASC, `ingredientId` ASC, `unitId` ASC),
  CONSTRAINT `fk_RecipeIngredient_Recipe`
    FOREIGN KEY (`recipeId`)
    REFERENCES `homecook`.`Recipe` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_RecipeIngredient_Ingredient`
    FOREIGN KEY (`ingredientId`)
    REFERENCES `homecook`.`Ingredient` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_RecipeIngredient_Unit`
    FOREIGN KEY (`unitId`)
    REFERENCES `homecook`.`Unit` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `homecook`.`ShoppingItemChecked`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `homecook`.`ShoppingItemChecked` ;

CREATE TABLE IF NOT EXISTS `homecook`.`ShoppingItemChecked` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `mealId` INT NOT NULL,
  `recipeIngredientId` INT NOT NULL,
  `quantityChecked` DOUBLE NOT NULL DEFAULT 0.0,
  PRIMARY KEY (`id`),
  INDEX `fk_ShoppingItemChecked_UserRecipe_idx` (`mealId` ASC),
  INDEX `fk_ShoppingItemChecked_RecipeIngredient_idx` (`recipeIngredientId` ASC),
  UNIQUE INDEX `ShoppingItemChecked_UNIQUE` (`mealId` ASC, `recipeIngredientId` ASC),
  CONSTRAINT `fk_ShoppingItemChecked_UserRecipe`
    FOREIGN KEY (`mealId`)
    REFERENCES `homecook`.`Meal` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ShoppingItemChecked_RecipeIngredient`
    FOREIGN KEY (`recipeIngredientId`)
    REFERENCES `homecook`.`RecipeIngredient` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `homecook`.`UserFavRecipe`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `homecook`.`UserFavRecipe` ;

CREATE TABLE IF NOT EXISTS `homecook`.`UserFavRecipe` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `userId` INT NOT NULL,
  `recipeId` INT NOT NULL,
  `rating` INT NULL,
  `isFav` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `fk_UserFavRecipeUser_idx` (`userId` ASC),
  INDEX `fk_UserFavRecipe_Recipe_idx` (`recipeId` ASC),
  UNIQUE INDEX `UNIQUE_User_Recipe` (`userId` ASC, `recipeId` ASC),
  CONSTRAINT `fk_UserFavRecipe_User`
    FOREIGN KEY (`userId`)
    REFERENCES `homecook`.`User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_UserFavRecipe_Recipe`
    FOREIGN KEY (`recipeId`)
    REFERENCES `homecook`.`Recipe` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `homecook`.`Plan`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `homecook`.`Plan` ;

CREATE TABLE IF NOT EXISTS `homecook`.`Plan` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `userId` INT NOT NULL,
  `title` VARCHAR(45) NOT NULL,
  `description` VARCHAR(1024) NOT NULL,
  `minPrice` DOUBLE NOT NULL,
  `maxPrice` DOUBLE NOT NULL,
  `active` TINYINT(1) NOT NULL DEFAULT 1,
  `planType` ENUM('Full', 'Breakfast', 'Lunch', 'Dinner') NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Plan_User_idx` (`userId` ASC),
  CONSTRAINT `fk_Plan_User`
    FOREIGN KEY (`userId`)
    REFERENCES `homecook`.`User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `homecook`.`PlanMeal`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `homecook`.`PlanMeal` ;

CREATE TABLE IF NOT EXISTS `homecook`.`PlanMeal` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `recipeId` INT NOT NULL,
  `planId` INT NOT NULL,
  `mealTime` ENUM('Breakfast', 'Lunch', 'Dinner') NOT NULL,
  `day` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_PlanMeal_Recipe_idx` (`recipeId` ASC),
  INDEX `fk_PlanMeal_Plan_idx` (`planId` ASC),
  CONSTRAINT `fk_PlanMeal_Recipe`
    FOREIGN KEY (`recipeId`)
    REFERENCES `homecook`.`Recipe` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_PlanMeal_Plan`
    FOREIGN KEY (`planId`)
    REFERENCES `homecook`.`Plan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `homecook`.`UserPlan`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `homecook`.`UserPlan` ;

CREATE TABLE IF NOT EXISTS `homecook`.`UserPlan` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `userId` INT NOT NULL,
  `planId` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_UserPlan_User_idx` (`userId` ASC),
  INDEX `fk_UserPlan_Plan_idx` (`planId` ASC),
  UNIQUE INDEX `UserPlan_UNIQUE` (`userId` ASC, `planId` ASC),
  CONSTRAINT `fk_UserPlan_User`
    FOREIGN KEY (`userId`)
    REFERENCES `homecook`.`User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_UserPlan_Plan`
    FOREIGN KEY (`planId`)
    REFERENCES `homecook`.`Plan` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `homecook`.`Group`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `homecook`.`Group` ;

CREATE TABLE IF NOT EXISTS `homecook`.`Group` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `homecook`.`UserFavGroup`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `homecook`.`UserFavGroup` ;

CREATE TABLE IF NOT EXISTS `homecook`.`UserFavGroup` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `userId` INT NOT NULL,
  `groupId` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_UserFavGroup_Group_idx` (`groupId` ASC),
  INDEX `fk_UserFavGroup_User_idx` (`userId` ASC),
  UNIQUE INDEX `UserFavGroup_UNIQUE` (`userId` ASC, `groupId` ASC),
  CONSTRAINT `fk_UserFavGroup_User`
    FOREIGN KEY (`userId`)
    REFERENCES `homecook`.`User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_UserFavGroup_Group`
    FOREIGN KEY (`groupId`)
    REFERENCES `homecook`.`Group` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `homecook`.`RecipeGroup`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `homecook`.`RecipeGroup` ;

CREATE TABLE IF NOT EXISTS `homecook`.`RecipeGroup` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `recipeId` INT NOT NULL,
  `groupId` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_RecipeGroup_Recipe_idx` (`recipeId` ASC),
  INDEX `fk_RecipeGroup_Group_idx` (`groupId` ASC),
  UNIQUE INDEX `RecipeGroup_UNIQUE` (`recipeId` ASC, `groupId` ASC),
  CONSTRAINT `fk_RecipeGroup_Recipe`
    FOREIGN KEY (`recipeId`)
    REFERENCES `homecook`.`Recipe` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_RecipeGroup_Group`
    FOREIGN KEY (`groupId`)
    REFERENCES `homecook`.`Group` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `homecook`.`Payment`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `homecook`.`Payment` ;

CREATE TABLE IF NOT EXISTS `homecook`.`Payment` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `userId` INT NOT NULL,
  `transactionId` VARCHAR(256) NULL,
  `payerEmail` VARCHAR(256) NULL,
  `payerId` INT NULL,
  `addressCity` VARCHAR(256) NULL,
  `addressCountry` VARCHAR(256) NULL,
  `firstName` VARCHAR(256) NULL,
  `lastName` VARCHAR(256) NULL,
  `payerStatus` VARCHAR(256) NULL,
  `paymentDate` DATE NULL,
  `paymentType` VARCHAR(256) NULL,
  `paymentStatus` VARCHAR(256) NULL,
  `itemNumber` VARCHAR(256) NULL,
  `itemName` VARCHAR(256) NULL,
  `grossValue` DECIMAL(10,2) NULL,
  `fee` DECIMAL(10,2) NULL,
  `currency` VARCHAR(256) NULL,
  `discount` DECIMAL(10,2) NULL,
  `selectedOption` VARCHAR(256) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_Payment_User_idx` (`userId` ASC),
  CONSTRAINT `fk_Payment_User`
    FOREIGN KEY (`userId`)
    REFERENCES `homecook`.`User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `homecook` ;

-- -----------------------------------------------------
-- Placeholder table for view `homecook`.`ShoppingItemView`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `homecook`.`ShoppingItemView` (`id` INT, `quantityChecked` INT, `date` INT, `mealTime` INT, `userId` INT, `recipeId` INT, `'mealServings'` INT, `ingredientId` INT, `'recipeQuantity'` INT, `unitId` INT, `'recipeServings'` INT);

-- -----------------------------------------------------
-- Placeholder table for view `homecook`.`AverageRecipeRating`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `homecook`.`AverageRecipeRating` (`recipeId` INT, `rating` INT);

-- -----------------------------------------------------
-- View `homecook`.`ShoppingItemView`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `homecook`.`ShoppingItemView` ;
DROP TABLE IF EXISTS `homecook`.`ShoppingItemView`;
USE `homecook`;
CREATE  OR REPLACE VIEW `ShoppingItemView` AS
    SELECT 
        sic.id,
        sic.quantityChecked,
        ur.date,
        ur.mealTime,
        ur.userId,
        ur.recipeId,
        ur.servings AS 'mealServings',
        ri.ingredientId,
        ri.quantity AS 'recipeQuantity',
        ri.unitId,
        r.servings AS 'recipeServings'
    FROM
        ShoppingItemChecked AS sic
            INNER JOIN
        Meal AS ur ON ur.id = sic.mealId
            INNER JOIN
        Recipe AS r ON r.id = ur.recipeId
            INNER JOIN
        RecipeIngredient AS ri ON ri.id = sic.recipeIngredientId;

-- -----------------------------------------------------
-- View `homecook`.`AverageRecipeRating`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `homecook`.`AverageRecipeRating` ;
DROP TABLE IF EXISTS `homecook`.`AverageRecipeRating`;
USE `homecook`;
CREATE  OR REPLACE VIEW `AverageRecipeRating` AS
    SELECT 
        Recipe.id AS recipeId,
        CAST(IFNULL(AVG(rating), 3.5) AS DECIMAL (18 , 1 )) AS rating
    FROM
        UserFavRecipe
            RIGHT JOIN
        Recipe ON UserFavRecipe.recipeId = Recipe.id
    GROUP BY Recipe.id;
USE `homecook`;

DELIMITER $$

USE `homecook`$$
DROP TRIGGER IF EXISTS `homecook`.`Meal_AFTER_INSERT` $$
USE `homecook`$$
CREATE DEFINER = CURRENT_USER TRIGGER `homecook`.`Meal_AFTER_INSERT` AFTER INSERT ON `Meal` FOR EACH ROW
BEGIN

INSERT INTO homecook.ShoppingItemChecked
  SELECT 
    null, 
    new.id, 
    RecipeIngredient.id AS 'recipeIngredientId',
        0
    
  FROM RecipeIngredient
    WHERE RecipeIngredient.recipeId = new.recipeId;


END$$


USE `homecook`$$
DROP TRIGGER IF EXISTS `homecook`.`Meal_BEFORE_DELETE` $$
USE `homecook`$$
CREATE DEFINER = CURRENT_USER TRIGGER `homecook`.`Meal_BEFORE_DELETE` BEFORE DELETE ON `Meal` FOR EACH ROW
BEGIN

DELETE FROM ShoppingItemChecked
WHERE ShoppingItemChecked.mealId = old.id;


END$$


USE `homecook`$$
DROP TRIGGER IF EXISTS `homecook`.`RecipeIngredient_BEFORE_DELETE` $$
USE `homecook`$$
CREATE DEFINER = CURRENT_USER TRIGGER `homecook`.`RecipeIngredient_BEFORE_DELETE` BEFORE DELETE ON `RecipeIngredient` FOR EACH ROW
BEGIN

DELETE FROM ShoppingItemChecked WHERE ShoppingItemChecked.recipeIngredientId = OLD.id;

END$$


DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;