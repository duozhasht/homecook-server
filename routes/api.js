var express = require('express');
var router = express.Router();
var swaggerJSDoc = require('swagger-jsdoc');
var jwt = require('jsonwebtoken');
var config = require('../config');

//API DOCS - swagger definition
var swaggerDefinition = {
  info: {
    title: 'homecook API',
    version: '1.0.0',
    description: 'Descrição da API homecook.',
  },
  host: '52.213.253.65',
  basePath: '/api',
};

// options for the swagger docs
var options = {
  // import swaggerDefinitions
  swaggerDefinition: swaggerDefinition,
  // path to the API docs
  apis: ['./routes/api/docs/*.js'],
};

// initialize swagger-jsdoc
var swaggerSpec = swaggerJSDoc(options);

// serve swagger
router.get('/swagger.json', function(req, res) {
  res.setHeader('Content-Type', 'application/json');
  res.send(swaggerSpec);
});

var auth = require('./auth');
var ingredientCategories = require('./api/ingredient-categories');
var ingredients = require('./api/ingredients');
var meals = require('./api/meals');
var recipes = require('./api/recipes');
var shoppingItems = require('./api/shopping-items');
var units = require('./api/units');
var users = require('./api/users');
var payments = require('./api/payments');
var plans = require('./api/plans');
var providers = require('./api/providers');
var groups = require('./api/groups');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send('This is our api');
});
/* Open Acess */
router.use('/auth', auth);

/* Authentication Required */
var isAuthenticated = function(req, res, next) {
  if(req.originalUrl.match(/\/api\/recipes\/[0-9]+/) && req.method == "GET"){
    //console.log(req._parsedOriginalUrl.path);
    req.user = {
      "id": 3,
      "username": "FreeUser",
      "email": "free@test.com",
      "role": {
        "id": 3,
        "role": "free"
      }
    }
    next();
  }
  else {
    jwt.verify(req.headers.token, config.secret , function(err, decoded) {
      if (err) {
        res.status('401').json('Requires Authentication');
      }
      else {
        req.user = decoded;
        next();
      }
    })
  }
}

router.use('/ingredientCategories', isAuthenticated, ingredientCategories);
router.use('/ingredients', isAuthenticated, ingredients);
router.use('/meals', isAuthenticated, meals);
router.use('/recipes', isAuthenticated, recipes);
router.use('/shoppingItems', isAuthenticated, shoppingItems);
router.use('/units', isAuthenticated, units);
router.use('/users', isAuthenticated, users);
router.use('/payments', isAuthenticated, payments);
router.use('/plans', isAuthenticated, plans);
router.use('/providers', isAuthenticated, providers);
router.use('/groups', isAuthenticated, groups);


module.exports = router;
