var express = require('express');
var router = express.Router();

var ProviderModule = require('../../models/provider').providerModule;
var PlanModule = require('../../models/plan').planModule;
/* GET recipes page. */
/* /api/recipes */

router.get('/plans', function(req, res, next) {
  PlanModule.findByOwner(req.user.id, function(err, result) {
    if (err) {
      res.status(err.code).json(err);
    }
    else {
      res.status(200).json(result);
    }
  });
});

router.get('/:id?', function(req, res, next) {
  if (req.params.id == undefined) {
    ProviderModule.findAll(function(err, result) {
      if (err) {
        res.status(err.code).json(err);
      }
      else {
        res.status(200).json(result);
      }
    });
    return;
  }
  else {
    ProviderModule.findById(req.params.id, function(err, result) {
      if (err) {
        res.status(err.code).json(err);
      }
      else {
        res.status(200).json(result);
      }
    });
    return;
  }
});

module.exports = router;
