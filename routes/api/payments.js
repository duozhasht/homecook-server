var express = require('express');
var router = express.Router();
var request = require('request');
var Promise = require('promise');

var PaymentModule = require('../../models/payment').paymentModule;
var Payment = require('../../models/payment').payment;
var createPaymentFromPayPal = require('../../models/payment').createPaymentFromPayPal;
var PlanModule = require('../../models/plan').planModule;
var SettingsModule = require('../../models/settings').settingsModule;

router.post('/paypal_pdt', function (req, res, next) {
  var authToken = "Bvlv8uty_iZJKK5JSX4nKRMe07isvhRaRz0hce-AcPmuD5g2jCy_84tU2Ou";
  var txToken = req.body.tx;
  var query = "&cmd=_notify-synch&tx=" + txToken + "&at=" + authToken;

  request({
    uri: 'https://www.paypal.com/cgi-bin/webscr' + query,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
    },
  }, function (error, response, body) {
    if (body.startsWith("SUCCESS")) {
      
      var payment = createPaymentFromPayPal(body);
      payment.userId = req.user.id;

      PaymentModule.create(payment, function (err, result) {
        if (err) {
          res.status(err.code).json({ message: err.msg, status: { code: err.code } });
        }
        else {

          if (payment.isSubscription()) {
            SettingsModule.promoteToPreemium(req.user.id, function (error, result) {
              if (error) {
                res.status(error.code).json({ message: error.msg, status: { code: error.code } });
              }
              else {
                res.status(200).json(result);
              }
            });
          }
          else {
            PlanModule.addPlanToUser(payment.itemId, req.user.id, function(error, result) {
              if (error) {
                res.status(error.code).json({ message: error.msg, status: { code: error.code } });
              }
              else {
                res.status(200).json(result);
              }
            });
          }
        }
      });
    }
    else {
      res.status(404).json(body);
    }
  });
});

module.exports = router;