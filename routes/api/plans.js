var express = require('express');
var router = express.Router();

var PlanModule = require('../../models/plan').planModule;
var PlanMealModule = require('../../models/plan-meal').planMealModule;
/* GET recipes page. */
/* /api/recipes */

router.get('/', function(req, res, next) {
  var limit = 10;
  var skip = 0;

  if (req.query.limit) {
    limit = req.query.limit;
  }

  if (req.query.skip) {
    skip = req.query.skip;
  }

  if (req.header('filter-owner')) {
    PlanModule.findByOwner(req.user.id, limit, skip, function(err, result) {
      if (err) {
        res.status(err.code).json(err);
      }
      else {
        res.status(200).json(result);
      }
    });
  }
  else if (req.header('filter-bought')) {
    PlanModule.findByBought(req.user.id, limit, skip, function(err, result) {
      if (err) {
        res.status(err.code).json(err);
      }
      else {
        res.status(200).json(result);
      }
    });
  }
  else if (req.header('filter-providerId')) {
    PlanModule.findByOwner(req.header('filter-providerId'), limit, skip, function(err, result) {
      if (err) {
        res.status(err.code).json(err);
      }
      else {
        res.status(200).json(result);
      }
    });
  }
  else {
    PlanModule.findAll(req.user.id, limit, skip, function(err, result) {
      if (err) {
        res.status(err.code).json(err);
      }
      else {
        res.status(200).json(result);
      }
    });
  }

});


router.get('/:id', function(req, res, next) {
  PlanModule.findById(req.params.id, req.user.id, function(err, result) {
    if (err) {
      res.status(err.code).json(err);
    }
    else {
      res.status(200).json(result);
    }
  });
});

router.post('/', function(req, res, next) {
  PlanModule.create(req.user.id, req.body.title, req.body.description, req.body.minPrice, req.body.maxPrice, req.body.planType, function(err, result) {
    if (err) {
      res.status(err.code).json(err);
    }
    else {
      res.status(200).json(result);
    }
  });
});

router.put('/:id', function(req, res, next) {
  PlanModule.update(req.params.id, req.user.id, req.body.title, req.body.description, req.body.minPrice, req.body.maxPrice, req.body.planType, function(err, result) {
    if (err) {
      res.status(err.code).json(err);
    }
    else {
      res.status(200).json(result);
    }
  });
});

router.delete('/:id', function(req, res, next) {
  PlanModule.delete(req.params.id, function(err, result) {
    if (err) {
      res.status(err.code).json(err);
    }
    else {
      res.status(200).json(result);
    }
  });
});


router.get('/:id/meals', function(req, res, next) {
  PlanMealModule.findByPlan(req.params.id, req.user.id, function(err, result) {
    if (err) {
      res.status(err.code).json(err);
    }
    else {
      res.status(200).json(result);
    }
  });
});

router.post('/:id/meals', function(req, res, next) {
  PlanMealModule.create(req.body.planId, req.body.day, req.body.mealTime, req.body.recipe.id, function(err, result) {
    if (err) {
      res.status(err.code).json(err);
    }
    else {
      res.status(200).json(result);
    }
  });
});

router.delete('/:id/meals/:mealId', function(req, res, next) {
  PlanMealModule.delete(req.params.mealId, function(err, result) {
    if (err) {
      res.status(err.code).json(err);
    }
    else {
      res.status(200).json(result);
    }
  });
});


module.exports = router;
