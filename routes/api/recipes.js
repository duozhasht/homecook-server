var express = require('express');
var router = express.Router();

var RecipeModule = require('../../models/recipe').recipeModule;
var GroupModule = require('../../models/group').groupModule;
/* GET recipes page. */
/* /api/recipes */

router.get('/', function (req, res, next) {

  var search = '';
  var limit = 100;
  var skip = 0;
  var isFavorite = (req.query.isFavorite === 'true');
  var groups = '';
  var isBreakfast = '';

  if (req.query.search) {
    search = req.query.search;
  }
  if (req.query.skip) {
    skip = req.query.skip;
  }
  if (req.query.limit) {
    limit = req.query.limit;
  }
  if (req.query.groups) {
    groups = req.query.groups;
  }
  if (req.query.isBreakfast) {
    isBreakfast = req.query.isBreakfast;
  }

  if (req.query.userid) {
    RecipeModule.findUserRecepies(req.user.id, req.query.userid, search, req.query.rating, req.query.price, req.query.time, skip, limit, groups, isBreakfast, function (err, recipes) {
      if (err) {
        res.status(err.code).json(err);
      }
      else {
        var promisseResults = recipes.map(function(recipe){
          return new Promise(function(resolve, reject){
            GroupModule.findByRecipeId(recipe.id, function(err, result){
              if (err){
                reject(err);
              }
              else {
                resolve(result);
              }
            });
          }).then(function(result){
            recipe.groups = result;
          });
        });

        Promise.all(promisseResults).then(function(results){
          res.status(200).json(recipes);
        }).catch(function(errors){
          res.status(500).json(errors);
        });
      }
    }); 
    return;
  }


  RecipeModule.findByName(req.user.id, search, isFavorite, req.query.rating, req.query.price, req.query.time, skip, limit, groups, isBreakfast, function (err, recipes) {
    if (err) {
      res.status(err.code).json(err);
    }
    else {
      var promisseResults = recipes.map(function(recipe){
        return new Promise(function(resolve, reject){
          GroupModule.findByRecipeId(recipe.id, function(err, result){
            if (err){
              reject(err);
            }
            else {
              resolve(result);
            }
          });
        }).then(function(result){
          recipe.groups = result;
        });
      });

      Promise.all(promisseResults).then(function(results){
        res.status(200).json(recipes);
      }).catch(function(errors){
        res.status(500).json(errors);
      });
    }
  });

});

router.get('/suggestions', function(req, res, next){
  RecipeModule.findSuggestions(req.user.id, function(err, recipes){
    if (err) {
      res.status(err.code).json(err);
    }
    else {
      var promisseResults = recipes.map(function(recipe){
        return new Promise(function(resolve, reject){
          GroupModule.findByRecipeId(recipe.id, function(err, result){
            if (err){
              reject(err);
            }
            else {
              resolve(result);
            }
          });
        }).then(function(result){
          recipe.groups = result;
        });
      });

      Promise.all(promisseResults).then(function(results){
        res.status(200).json(recipes);
      }).catch(function(errors){
        res.status(500).json(errors);
      });
    }
  });
});

router.get('/:id', function(req, res, next){
  RecipeModule.findById(req.params.id, req.user.id, function(err, recipe){
    if (err) {
      res.status(err.code).json(err);
    }
    else {
      new Promise(function(resolve, reject){
        GroupModule.findByRecipeId(recipe.id, function(err, result){
          if (err){
            reject(err);
          }
          else {
            resolve(result);
          }
        });
      }).then(function(result){
        recipe.groups = result;
        res.status(200).json(recipe);          
      }).catch(function(err){
        res.status(err.code).json(err);
      });
    }
  });
});

router.post('/', function(req, res, next) {
  
  if (!req.body.name || !req.body.description || !req.body.servings || !req.body.price || !req.body.time || !req.body.difficulty || !req.body.showDescription || !req.body.isBreakfast) {
    res.status(400).json({ message: 'Bad Request', status: { code: 400 } });
  }
  else {
    access = "public";
    if (req.body.access) {
      access = req.body.access;
    }
    RecipeModule.create(req.body.name, req.body.description, req.body.image, req.body.servings, req.body.price, req.body.time, req.body.difficulty, req.user.id, access, req.body.showDescription, req.body.isBreakfast,
      function (err, result) {
        if (err) {
          res.status(500).json(err);
        }
        else {
          res.status(200).json(result);
        }
      });
  }
});

router.put('/:id', function (req, res, next) {

  if (!req.body.name || !req.body.description || !req.body.image || !req.body.servings || !req.body.price || !req.body.time || !req.body.difficulty || !req.body.showDescription) {
    res.status(400).json({ message: 'Bad Request', status: { code: 400 } });
  }
  else {
    var isBreakfast = 0;
    if (req.body.isBreakfast)
    {
      isBreakfast = req.body.isBreakfast;
    }
    RecipeModule.update(req.params.id, req.body.name, req.body.description, req.body.image, req.body.servings, req.body.rating, req.body.price, req.body.time, req.body.difficulty, req.body.showDescription, isBreakfast,
      function (err, result) {
        if (err) {
          res.status(500).json(err);
        }
        else {
          res.status(200).json(result);
        }
      });
  }
});

router.patch('/:id', function (req, res, next) {
  if (req.body.isFavorite !== undefined) {
    RecipeModule.favorite(req.user.id, req.params.id, req.body.isFavorite, function (err, result) {
      if (err) {
        res.status(err.code).json(err);
      }
      else {
        res.status(200).json(result);
      }
    })
  }
});

router.get('/:id/ingredients', function (req, res, next) {
  RecipeModule.findRecipeIngredients(req.params.id, function (err, result) {
    if (err) {
      res.status(err.code).json(err);
    }
    else {
      res.status(200).json(result);
    }
  });
});

router.post('/:id/ingredients', function (req, res, next) {
  if (!req.body.ingredient.id || !req.body.unit.id || !req.body.quantity) {
    res.status(400).json({ message: 'Bad Request', status: { code: 400 } });
    return;
  }

  RecipeModule.addIngredientLine(req.params.id, req.body.ingredient.id, req.body.unit.id, req.body.quantity, function (err, result) {
    if (err) {
      res.status(err.code).json(err);
    }
    else {
      res.status(200).json(result);
    }
  });
});

router.put('/:id/ingredients/:ingredientLineId', function(req, res, next) {
  if (!req.body.ingredient.id || !req.body.unit.id || !req.body.quantity) {
    res.status(400).json({
      message: 'Bad Request',
      status: {
        code: 400
      }
    });
    return;
  }
  RecipeModule.updateIngredientLine(req.params.id, req.params.ingredientLineId, req.body.ingredient.id, req.body.unit.id, req.body.quantity, function(err, result) {
    if (err) {
      res.status(err.code).json(err);
    } else {
      res.status(200).json(result);
    }
  });
});

router.get('/:id/groups', function (req, res, next) {
  RecipeModule.findRecipeGroups(req.params.id, function (err, result) {
    if (err) {
      res.status(err.code).json(err);
    }
    else {
      res.status(200).json(result);
    }
  });
});

router.post('/:id/groups', function(req, res, next) {
    if (!req.body.groups) {
        res.status(400).json({ message: 'Bad Request', status: { code: 400 } });
        return;
    }
    new Promise((resolve, reject) => {
        RecipeModule.deleteGroups(req.params.id, req.body.groups, function(err, result) {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
    })
    .then(result => {
        return RecipeModule.addGroups(req.params.id, req.body.groups, result);
    })
    .then(result => {
        RecipeModule.findRecipeGroups(req.params.id, function (err, result) {
        if (err) {
          res.status(err.code).json(err);
        }
        else {
          res.status(200).json(result);
        }
      });
    })
    .catch(err => {
        res.status(500).send(err);
    });

    
});

// router.post('/:id/groups', function (req, res, next) {
//   if (!req.body.id) {
//     res.status(400).json({ message: 'Bad Request', status: { code: 400 } });
//     return;
//   }

//   RecipeModule.addGroup(req.params.id, req.body.id, function (err, result) {
//     if (err) {
//       res.status(err.code).json(err);
//     }
//     else {
//       res.status(200).json(result);
//     }
//   });
// });

router.delete('/:id', function (req, res, next) {
  RecipeModule.delete(req.params.id, function (err, result) {
    if (err) {
      res.status(err.code).json(err);
    }
    else {
      res.status(200).json(result);
    }
  })
});

router.delete('/ingredients/:ingredientLineId', function (req, res, next) {
  RecipeModule.deleteIngredientLine(req.params.ingredientLineId, function (err, result) {
    if (err) {
      res.status(err.code).json(err);
    }
    else {
      res.status(200).json(result);
    }
  });
});



module.exports = router;
