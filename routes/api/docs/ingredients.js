/**
 * @swagger
 * definitions:
 *   Ingredient:
 *     properties:
 *       id:
 *         description: Ingredient Id.
 *         type: integer
 *       name:
 *         description: Ingredient description.
 *         type: string
 *       categoryId:
 *         description: Ingredient category id.
 *         type: integer
 * paths:
 *  /ingredients/:
 *    get:
 *      tags:
 *        - Ingredients
 *      description: |
 *        Returns all Ingredients.
 *      responses:
 *        200:
 *          description: An array with any number of Ingredients.
 *          schema:
 *            type: array
 *            items:
 *              $ref: '#/definitions/Ingredient'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 *    post:
 *      tags:
 *        - Ingredients
 *      description: |
 *        Creates a new Ingredient.
 *      parameters:
 *        - name: name
 *          in: body
 *          description: Ingredient description.
 *          required: true
 *          type: string
 *        - name: categoryId
 *          in: body
 *          description: Ingredient Category Id.
 *          required: true
 *          type: integer
 *      responses:
 *        200:
 *          description: An object with the created Ingredient.
 *          schema:
 *            $ref: '#/definitions/Ingredient'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 *  /ingredients/{id}:
 *    get:
 *      tags:
 *        - Ingredients
 *      description: |
 *        Returns all Ingredients contained in a specific Category.
 *      parameters:
 *        - name: id
 *          in: path
 *          description: Category Id.
 *          required: true
 *          type: integer
 *      responses:
 *        200:
 *          description: An array with any number of Ingredients.
 *          schema:
 *            type: array
 *            items:
 *              $ref: '#/definitions/Ingredient'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 *    put:
 *      tags:
 *        - Ingredients
 *      description: |
 *        Updates an existing Ingredient.
 *      parameters:
 *        - name: id
 *          in: path
 *          description: Ingredient Id.
 *          required: true
 *          type: integer
 *        - name: name
 *          in: body
 *          description: Ingredient description.
 *          required: true
 *          type: string
 *        - name: categoryId
 *          in: body
 *          description: Ingredient Category Id.
 *          required: true
 *          type: integer
 *      responses:
 *        200:
 *          description: An object with the updated Ingredient.
 *          schema:
 *            $ref: '#/definitions/Ingredient'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 */