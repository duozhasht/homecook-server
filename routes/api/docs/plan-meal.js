/**
 * @swagger
 * definitions:
 *  PlanMeal:
 *    type: object
 *    properties:
 *      id:
 *        type: integer
 *      planId:
 *        type: integer
 *      day:
 *        type: int
 *      mealTime:
 *        $ref: '#/definitions/mealTime'
 *      recipe:
 *        $ref: '#/definitions/Recipe'
 */