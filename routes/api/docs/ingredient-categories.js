/**
 * @swagger
 * definitions:
 *   IngredientCategory:
 *     properties:
 *       id:
 *         description: Ingredient category Id.
 *         type: integer
 *       name:
 *         description: Ingredient category description.
 *         type: string
 * paths:
 *  /ingredientCategories:
 *    get:
 *      tags:
 *        - Ingredient Categories
 *      description: |
 *        Returns all Ingredient Categories.
 *      responses:
 *        200:
 *          description: An array with any number of ingredient categories.
 *          schema:
 *            type: array
 *            items:
 *              $ref: '#/definitions/IngredientCategory'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 *    post:
 *      tags:
 *        - Ingredient Categories
 *      description: |
 *        Creates new Ingredient Category.
 *      parameters:
 *        - name: name
 *          in: body
 *          description: Category description.
 *          required: true
 *          type: string
 *      responses:
 *        200:
 *          description: An object with the created ingredient category.
 *          schema:
 *            $ref: '#/definitions/IngredientCategory'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 *  /ingredientCategories/{id}:
 *    put:
 *      tags:
 *        - Ingredient Categories
 *      description: |
 *        Updates an existing Ingredient Category.
 *      parameters:
 *        - name: id
 *          in: path
 *          description: Category Id.
 *          required: true
 *          type: integer
 *        - name: name
 *          in: body
 *          description: Category description.
 *          required: true
 *          type: string
 *      responses:
 *        200:
 *          description: An object with the updated ingredient categories.
 *          schema:
 *            $ref: '#/definitions/IngredientCategory'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 */