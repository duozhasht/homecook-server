/**
 * @swagger
 * definitions:
 *  Settings:
 *    type: object
 *    properties:
 *      username:
 *        type: string
 *      email:
 *        type: string
 *        format: email
 *      password:
 *        type: string
 *        format: password
 *      servings:
 *        type: integer
 *        format: int32
 *      role:
 *        type: integer
 *        format: int32
 *      groups:
 *        schema:
 *          type: array
 *          $ref: '#/definitions/UserFavGroup'
 */