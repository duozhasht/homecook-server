/**
 * @swagger
 * definitions:
 *  Meal:
 *    type: object
 *    properties:
 *      id:
 *        type: integer
 *        description: Id of the meal
 *      date:
 *        type: string
 *        format: date
 *        description: Day of the meal
 *      mealTime:
 *        $ref: '#/definitions/mealTime'
 *        description: breakfast, lunch or dinner
 *      servings:
 *        type: number
 *        description: Number of servings for the meal
 *      recipe:
 *        $ref: '#/definitions/Recipe'
 *        description: Recipe planned for the meal
 *  mealTime:
 *    type: string
 *    enum: 
 *      - breakfast
 *      - lunch
 *      - dinner
 * paths:
 *  /meals:
 *    get:
 *      tags:
 *        - Meals
 *      description: |
 *        Returns all meals between filter-from and filter-until.
 *      parameters:
 *        - name: filter-from
 *          in: header
 *          description: Initial date to be consulted.
 *          required: true
 *          schema: 
 *            type: string
 *            format: date
 *        - name: filter-until
 *          in: header
 *          description: Final date to be consulted.
 *          required: true
 *          schema: 
 *            type: string
 *            format: date
 *      responses:
 *        200:
 *          description: An array with any number of meals.
 *          schema:
 *            type: array
 *            items:
 *              $ref: '#/definitions/Meal'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 *    post:
 *      tags:
 *        - Meals
 *      description: |
 *        Inserts a new meal.
 *      parameters:
 *        - name: Meal
 *          in: body
 *          description: Meal to be inserted.
 *          required: true
 *          schema: 
 *            $ref: '#/definitions/Meal'
 *      responses:
 *        200:
 *          description: Object with the new Meal.
 *          schema:
 *              $ref: '#/definitions/Meal'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 *  /meals/{id}:
 *    put:
 *      tags:
 *        - Meals
 *      description: |
 *        Updates one meal.
 *      parameters:
 *        - name: id
 *          in: query
 *          description: Id of the meal to be updated.
 *          required: true
 *          type: number
 *        - name: meal
 *          in: body
 *          description: Meal to be updated.
 *          required: true
 *          schema: 
 *            $ref: '#/definitions/Meal'
 *      responses:
 *        200:
 *          description: Id of the updated object
 *          schema:
 *            type: number
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'  
 *    delete:
 *      tags:
 *        - Meals
 *      description: |
 *        Deletes one meal identified by an id.
 *      parameters:
 *        - name: id
 *          in: query
 *          description: Id of the meal to be deleted.
 *          required: true
 *          type: integer
 *      responses:
 *        200:
 *          description: Id of the deleted meal.
 *          schema:
 *            type: integer
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error' 
 */