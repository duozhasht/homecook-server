/**
 * @swagger
 * definitions:
 *   Unit:
 *     properties:
 *       id:
 *         description: Unit Id.
 *         type: integer
 *       name:
 *         description: Unit description.
 *         type: string
 *       aggregate:
 *         description: Wether the Unit can be aggregated.
 *         type: boolean
 * paths:
 *  /units:
 *    get:
 *      tags:
 *        - Units
 *      description: |
 *        Returns all Units.
 *      responses:
 *        200:
 *          description: An array with any number of Units.
 *          schema:
 *            type: array
 *            items:
 *              $ref: '#/definitions/Unit'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 *    post:
 *      tags:
 *        - Units
 *      description: |
 *        Inserts a new Unit.
 *      parameters:
 *        - name: name
 *          in: body
 *          description: Unit description to be inserted.
 *          required: true
 *          type: string
 *        - name: aggregate
 *          in: body
 *          description: Wether the Unit can be aggregated.
 *          required: true
 *          type: boolean
 *      responses:
 *        200:
 *          description: Id of the new unit.
 *          schema:
 *            type: integer
 *        default:
 *          description: Unexpected error.
 *          schema:
 *            $ref: '#/definitions/Error'
 *  /units/{id}:
 *    get:
 *      tags:
 *        - Units
 *      description: |
 *        Returns one Unit identified by id.
 *      parameters:
 *        - name: id
 *          in: path
 *          description: Unit Id.
 *          required: true
 *          type: integer
 *      responses:
 *        200:
 *          description: An object with one Unit.
 *          schema:
 *            $ref: '#/definitions/Unit'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 *    put:
 *      tags:
 *        - Units
 *      description: |
 *        Updates one Unit
 *      parameters:
 *        - name: id
 *          in: path
 *          description: Id of the Unit to be updated.
 *          required: true
 *          type: integer
 *        - name: name
 *          in: body
 *          description: Unit description to be updated.
 *          required: true
 *          type: string
 *        - name: aggregate
 *          in: body
 *          description: Wether the unit can be aggregated.
 *          required: true
 *          type: boolean
 *      responses:
 *        200:
 *          description: Id of the updated Unit.
 *          schema:
 *            type: integer
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 */