/**
 * @swagger
 * definitions:
 *  Error:
 *    type: object
 *    properties:
 *      message:
 *        type: string
 *      status:
 *        type: object
 */