/**
 * @swagger
 * definitions:
 *   Provider:
 *    type: object
 *    properties:
 *      user:
 *        description: User's role.
 *        $ref: '#/definitions/User'
 *      job:
 *        type: string
 *        description: Provider's job.
 *      description:
 *        type: string
 *        description: Description of the User.
 *      image:
 *        type: string
 *        format: url
 *        description: Url for the avatar of the User.
 * paths:
 *  /providers/plans:
 *    get:
 *      tags:
 *        - Providers
 *      description: |
 *        Returns all Plans of the user.
 *      responses:
 *        200:
 *          description: An array with any number of Providers.
 *          schema:
 *            type: array
 *            items:
 *              $ref: '#/definitions/Plan'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 *  /providers:
 *    get:
 *      tags:
 *        - Providers
 *      description: |
 *        Returns all the Providers.
 *      parameters:
 *        - name: id
 *          in: path
 *          description: Provider's id.
 *          required: true
 *          type: integer
 *      responses:
 *        200:
 *          description: An object with the Providers.
 *          schema:
 *            $ref: '#/definitions/Provider'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 *  /providers/{id}:
 *    get:
 *      tags:
 *        - Providers
 *      description: |
 *        Returns the Provider identified by id.
 *      parameters:
 *        - name: id
 *          in: path
 *          description: Provider's id.
 *          required: true
 *          type: integer
 *      responses:
 *        200:
 *          description: An object with the Providers.
 *          schema:
 *            $ref: '#/definitions/Provider'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 */