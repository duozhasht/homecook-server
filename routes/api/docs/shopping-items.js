/**
 * @swagger
 * definitions:
 *   ShoppingItem:
 *     properties:
 *       id:
 *         description: Shopping Item ID.
 *         type: integer
 *       date:
 *         description: Date of meal using ingredient.
 *         format: date-time
 *         type: string
 *       recipeId:
 *         description: Recipe ID.
 *         type: integer
 *       recipeServings:
 *         description: Default number of servings for the recipe.
 *         type: integer
 *       mealTime:
 *         description: The time of the meal (breakfast, lunch, dinner).
 *         $ref: '#/definitions/mealType'
 *       mealServings:
 *         description: The number of servings for this meal.
 *         type: integer
 *       recipeQuantity:
 *         description: The quantity of this ingredient needed for the meal.
 *         type: number
 *       quantityChecked:
 *         description: The quantity of this ingredient already bought.
 *         type: number
 *       ingredient:
 *         description: The ingredient data.
 *         $ref: '#/definitions/Ingredient'
 *       unit:
 *         description: The unit of the ingredient quantity.
 *         $ref: '#/definitions/Unit' 
 *   ShoppingItemInput:
 *     properties:
 *       id:
 *         description: Shopping Item Id.
 *         required: true
 *         type: integer
 *       quantityChecked:
 *         type: number
 *         description: Updated quantity of the ingredient.
 *         required: true
 * paths:
 *  /shoppingItems:
 *    get:
 *       tags:
 *        - Shopping Items
 *       description: |
 *         Gets an array of shoppingItem objects.
 *       responses:
 *         200:
 *           description: Successful response
 *           schema:
 *             type: array
 *             items:
 *               $ref: '#/definitions/ShoppingItem' 
 *         default:
 *           description: Unexpected error
 *           schema:
 *             $ref: '#/definitions/Error'
 *    put:
 *       tags:
 *        - Shopping Items
 *       description: |
 *         Bulk updates an array of shoppingItem objects.
 *       parameters:
 *        - in: body
 *          description: Array of elements.
 *          required: true
 *          type: array
 *          items:
 *            $ref: '#/definitions/ShoppingItemInput' 
 *       responses:
 *         200:
 *           description: Id of the updated shopping item.
 *           schema:
 *             type: integer
 *         default:
 *           description: Unexpected error
 *           schema:
 *             $ref: '#/definitions/Error'
 *  /shoppingItems/{id}:
 *    put:
 *       tags:
 *        - Shopping Items
 *       description: |
 *         Updates a shoppingItem object identified by id.
 *       parameters:
 *        - name: id
 *          in: path
 *          description: Shopping Item Id.
 *          required: true
 *          type: integer
 *        - name: quantityChecked
 *          type: number
 *          in: body
 *          description: Updated quantity of the ingredient.
 *          required: true
 *       responses:
 *         200:
 *           description: Id of the updated shopping item.
 *           schema:
 *             type: integer
 *         default:
 *           description: Unexpected error
 *           schema:
 *             $ref: '#/definitions/Error'
 */