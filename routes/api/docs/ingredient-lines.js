/**
 * @swagger
 * definitions:
 *   IngredientLine:
 *     properties:
 *       id:
 *         description: IngredientLine Id.
 *         type: integer
 *       quantity:
 *         description: Recipe's Ingredient quantity.
 *         type: number
 *         format: double
 *       unit:
 *         description: Ingredient Unit.
 *         schema:
 *           $ref: '#/definitions/Unit'
 *       ingredient:
 *         description: Ingredient.
 *         schema:
 *           $ref: '#/definitions/Ingredient'
 *       recipeId:
 *         description: Recipe Id.
 *         type: integer
 */