/**
 * @swagger
 * definitions:
 *  User:
 *    type: object
 *    properties:
 *      id:
 *        type: integer
 *        description: User's id.
 *      username:
 *        type: string
 *        description: User's username.
 *      email:
 *        type: string
 *        format: email
 *        description: User's email.
 *      password:
 *        type: string
 *        format: password
 *        description: User's password.
 *      servings:
 *        type: integer
 *        description: User's default number of servings.
 *      role:
 *        description: User's role.
 *        $ref: '#/definitions/Role'
 * paths:
 *  /users:
 *    get:
 *      tags:
 *        - Users
 *      description: |
 *        Returns all Users.
 *      responses:
 *        200:
 *          description: An array with any number of Users.
 *          schema:
 *            type: array
 *            items:
 *              $ref: '#/definitions/User'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 *    post:
 *      tags:
 *        - Users
 *      description: |
 *        Creates one User.
 *      parameters:
 *        - name: username
 *          in: body
 *          description: User's username.
 *          required: true
 *          type: string
 *        - name: email
 *          in: body
 *          description: User's email.
 *          required: true
 *          type: string
 *          format: email
 *        - name: password
 *          in: body
 *          description: User's password.
 *          required: true
 *          type: string
 *          format: password
 *        - name: servings
 *          in: body
 *          description: User's default number of servings.
 *          required: true
 *          type: integer
 *        - name: role
 *          in: body
 *          description: User's role.
 *          required: true
 *          type: object
 *          schema:
 *            $ref: '#/definitions/Role'
 *      responses:
 *        200:
 *          description: An object with the created User.
 *          schema:
 *            type: array
 *            items:
 *              $ref: '#/definitions/User'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 *  /users/{id}:
 *    delete:
 *      tags:
 *        - Users
 *      description: |
 *        Removes the User identified by id.
 *      parameters:
 *        - name: id
 *          in: path
 *          description: User's id.
 *          required: true
 *          type: integer
 *      responses:
 *        200:
 *          description: Id of the deleted User.
 *          schema:
 *            $ref: '#/definitions/User'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 *  /users/settings:
 *    get:
 *      tags:
 *        - Users
 *      description: |
 *        Allows user to update its Settings.
 *      responses:
 *        200:
 *          description: An object with the User's settings.
 *          schema:
 *            $ref: '#/definitions/Settings'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 *    post:
 *      tags:
 *        - Users
 *      description: |
 *        Allows user to update its Settings.
 *      parameters:
 *        - name: username
 *          in: body
 *          description: User's new username.
 *          required: true
 *          type: string
 *        - name: email
 *          in: body
 *          description: User's new email.
 *          required: true
 *          type: string
 *          format: email
 *        - name: password
 *          in: body
 *          description: User's new password.
 *          required: false
 *          type: string
 *          format: password
 *        - name: servings
 *          in: body
 *          description: User's new number of servings.
 *          required: true
 *          type: integer
 *      responses:
 *        200:
 *          description: An object with the updated User.
 *          schema:
 *            $ref: '#/definitions/Settings'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 *  /users/settings/groups:
 *    get:
 *      tags:
 *        - Users
 *      description: |
 *        Allows user to update its Settings.
 *      responses:
 *        200:
 *          description: An array with all the User's favorite Groups.
 *          schema:
 *            type: array
 *            items:
 *              $ref: '#/definitions/UserFavGroup'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 *    post:
 *      tags:
 *        - Users
 *      description: |
 *        Allows user to add a favorite Groups.
 *      parameters:
 *        - name: UserFavGroup
 *          in: body
 *          description: User's new favorite Group.
 *          required: true
 *          schema:
 *            $ref: '#/definitions/UserFavGroup'
 *      responses:
 *        200:
 *          description: An array with all the User's favorite Groups.
 *          schema:
 *            type: array
 *            items:
 *              $ref: '#/definitions/UserFavGroup'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 *  /users/settings/{groupId}:
 *    delete:
 *      tags:
 *        - Users
 *      description: |
 *        Allows user to delete a favorite Groups.
 *      parameters:
 *        - name: groupId
 *          in: body
 *          description: User's favorite Group to be removed.
 *          required: true
 *          type: integer
 *      responses:
 *        200:
 *          description: An empty object.
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 *  /users/{id}/groups:
 *    get:
 *      tags:
 *        - Users
 *      description: |
 *        Get User's Groups.
 *      parameters:
 *        - name: id
 *          in: path
 *          description: User's id.
 *          required: true
 *          type: integer
 *      responses:
 *        200:
 *          description: An array of objects with the User's Groups.
 *          schema:
 *            type: array
 *            items:
 *              $ref: '#/definitions/Group'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 *    post:
 *      tags:
 *        - Users
 *      description: |
 *        Add a Group to the User.
 *      parameters:
 *        - name: id
 *          in: path
 *          description: User's id.
 *          required: true
 *          type: integer
 *        - name: groupId
 *          in: body
 *          description: Group's id.
 *          required: true
 *          type: integer
 *      responses:
 *        200:
 *          description: An array of objects with the User's Groups.
 *          schema:
 *            type: array
 *            items:
 *              $ref: '#/definitions/Group'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 */