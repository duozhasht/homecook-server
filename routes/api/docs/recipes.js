/**
 * @swagger
 * definitions:
 *   Recipe:
 *    type: object
 *    properties:
 *      id:
 *        type: integer
 *        description: Id of the recipe.
 *      name:
 *        type: string
 *        description: Name of the recipe.
 *      description:
 *        type: string
 *        description: Description of the recipe.
 *      image:
 *        type: string
 *        format: URL
 *        description: Url for the imge that of the recipe.
 *      servings:
 *        type: integer
 *        description: Number of servings of the recipe.
 *      rating:
 *        type: number
 *        description: Rating of the recipe.
 *      price:
 *        type: number
 *        description: Aproximated price of the recipe.
 *      time:
 *        type: number
 *        description: Aproximated time necessary to prepare the recipe.
 *      userId:
 *        type: integer
 *        description: User Id identifying the recipe's author.
 *      difficulty:
 *        type: string
 *        description: Recipe's degree of difficulty.
 *        enum: ['1', '2', '3', '4', '5']
 *      access:
 *        type: string
 *        description: Recipe's level of access.
 *        enum: ['private', 'restricted', 'public']
 *      shortDescription:
 *        type: string
 *        description: Short description of the recipe.
 * paths:
 *  /recipes:
 *    get:
 *      tags:
 *        - Recipes
 *      description: |
 *        Returns all Recipes.
 *      parameters:
 *        - name: userid
 *          in: query
 *          description: User's Id of the recipe author.
 *          required: false
 *          type: integer
 *        - name: search
 *          in: query
 *          description: Search parameter for the recipe.
 *          required: false
 *          type: string
 *        - name: skip
 *          in: query
 *          description: Position to start the recipe search.
 *          required: false
 *          type: integer
 *        - name: limit
 *          in: query
 *          description: Number of records to be returned.
 *          required: false
 *          type: integer
 *      responses:
 *        200:
 *          description: An array with any number of Recipes.
 *          schema:
 *            type: array
 *            items:
 *              $ref: '#/definitions/Recipe'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 *    post:
 *      tags:
 *        - Recipes
 *      description: |
 *        Creates a new Recipe.
 *      parameters:
 *        - name: name
 *          type: string
 *          in: body
 *          description: Name of the recipe.
 *          required: true
 *        - name: description
 *          type: string
 *          in: body
 *          description: Description of the recipe.
 *          required: true
 *        - name: image
 *          type: string
 *          in: body
 *          format: URL
 *          description: Url for the imge that of the recipe.
 *          required: true
 *        - name: servings
 *          type: integer
 *          in: body
 *          description: Number of servings of the recipe.
 *          required: true
 *        - name: rating
 *          type: number
 *          in: body
 *          description: Rating of the recipe.
 *          required: true
 *        - name: price
 *          type: number
 *          in: body
 *          description: Aproximated price of the recipe.
 *          required: true
 *        - name: time
 *          type: number
 *          in: body
 *          description: Aproximated time necessary to prepare the recipe.
 *          required: true
 *        - name: userId
 *          type: integer
 *          in: body
 *          description: User Id identifying the recipe's author.
 *          required: true
 *        - name: difficulty
 *          type: string
 *          in: body
 *          description: Recipe's degree of difficulty.
 *          enum: ['1', '2', '3', '4', '5']
 *          required: true
 *        - name: access
 *          type: string
 *          in: body
 *          description: Recipe's level of access.
 *          enum: ['private', 'restricted', 'public']
 *        - name: shortDescription
 *          type: string
 *          in: body
 *          description: Short description of the recipe.
 *          required: true
 *      responses:
 *        200:
 *          description: An object with the created recipe.
 *          schema:
 *            $ref: '#/definitions/Recipe'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 *  /recipes/suggestions:
 *    get:
 *      tags:
 *        - Recipes
 *      description: |
 *        Returns a number of suggested Recipes.
 *      responses:
 *        200:
 *          description: An array with any number of Recipes.
 *          schema:
 *            type: array
 *            items:
 *              $ref: '#/definitions/Recipe'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 *  /recipes/{id}:
 *    get:
 *      tags:
 *        - Recipes
 *      description: |
 *        Returns the Recipe identified by id.
 *      parameters:
 *        - name: id
 *          in: path
 *          description: Id of the Recipe.
 *          required: true
 *          type: integer
 *      responses:
 *        200:
 *          description: An object of type Recipe.
 *          schema:
 *            $ref: '#/definitions/Recipe'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 *    patch:
 *      tags:
 *        - Recipes
 *      description: |
 *        Sets or unsets the Recipe identified by id as favorite.
 *      parameters:
 *        - name: id
 *          in: path
 *          description: Id of the Recipe.
 *          required: true
 *          type: integer
 *        - name: isFavorite
 *          in: body
 *          description: Wether the Recipe is a favorite or not.
 *          required: true
 *          type: boolean
 *      responses:
 *        200:
 *          description: An empty object.
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 *    put:
 *      tags:
 *        - Recipes
 *      description: |
 *        Updates one Recipe identified by id.
 *      parameters:
 *        - name: id
 *          in: path
 *          description: Recipes Id.
 *          required: true
 *          type: integer
 *        - name: name
 *          type: string
 *          in: body
 *          description: Name of the recipe.
 *          required: true
 *        - name: description
 *          type: string
 *          in: body
 *          description: Description of the recipe.
 *          required: true
 *        - name: image
 *          type: string
 *          in: body
 *          format: URL
 *          description: Url for the imge that of the recipe.
 *          required: true
 *        - name: servings
 *          type: integer
 *          in: body
 *          description: Number of servings of the recipe.
 *          required: true
 *        - name: rating
 *          type: number
 *          in: body
 *          description: Rating of the recipe.
 *          required: true
 *        - name: price
 *          type: number
 *          in: body
 *          description: Aproximated price of the recipe.
 *          required: true
 *        - name: time
 *          type: number
 *          in: body
 *          description: Aproximated time necessary to prepare the recipe.
 *          required: true
 *        - name: userId
 *          type: integer
 *          in: body
 *          description: User Id identifying the recipe's author.
 *          required: true
 *        - name: difficulty
 *          type: string
 *          in: body
 *          description: Recipe's degree of difficulty.
 *          enum: ['1', '2', '3', '4', '5']
 *          required: true
 *        - name: access
 *          type: string
 *          in: body
 *          description: Recipe's level of access.
 *          enum: ['private', 'restricted', 'public']
 *        - name: shortDescription
 *          type: string
 *          in: body
 *          description: Short description of the recipe.
 *          required: true
 *      responses:
 *        200:
 *          description: An object with the updated Recipe.
 *          schema:
 *            $ref: '#/definitions/Recipe'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error' 
 *    delete:
 *      tags:
 *        - Recipes
 *      description: |
 *        Deletes one recipe identified by an id.
 *      parameters:
 *        - name: id
 *          in: query
 *          description: Id of the Recipe to be deleted.
 *          required: true
 *          type: integer
 *      responses:
 *        200:
 *          description: Id of the deleted meal.
 *          schema:
 *            $ref: '#/definitions/Recipe'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error' 
 *  /recipes/{id}/ingredients:
 *    get:
 *      tags:
 *        - Recipes
 *      description: |
 *        Returns the Ingredients of one Recipe identified by id.
 *      parameters:
 *        - name: id
 *          in: path
 *          description: Recipe Id.
 *          required: true
 *          type: integer
 *      responses:
 *        200:
 *          description: An object with one Recipes.
 *          schema: 
 *            type: array
 *            items:
 *              $ref: '#/definitions/IngredientLine'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 *    post:
 *      tags:
 *        - Recipes
 *      description: |
 *        Inserts a new ingredientline on the Recipe identified by id.
 *      parameters:
 *        - name: id
 *          in: path
 *          description: Recipe Id.
 *          required: true
 *          type: integer
 *        - name: id
 *          in: body
 *          description: Ingredient Id.
 *          required: true
 *          type: integer
 *        - name: unit
 *          in: body
 *          description: Ingredient Unit.
 *          required: true
 *          schema:
 *            $ref: '#/definitions/Unit'
 *        - name: quantity
 *          in: body
 *          description: Ingredient quantity.
 *          type: number
 *          format: double
 *      responses:
 *        200:
 *          description: An array of IngredientLine for the Recipe.
 *          schema: 
 *            type: array
 *            items:
 *              $ref: '#/definitions/IngredientLine'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 *  /recipes/{id}/groups:
 *    get:
 *      tags:
 *        - Recipes
 *      description: |
 *        Returns the Groups of one Recipe identified by id.
 *      parameters:
 *        - name: id
 *          in: path
 *          description: Recipe Id.
 *          required: true
 *          type: integer
 *      responses:
 *        200:
 *          description: An object with one Recipes.
 *          schema: 
 *            type: array
 *            items:
 *              $ref: '#/definitions/Group'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 *    post:
 *      tags:
 *        - Recipes
 *      description: |
 *        Inserts a new Group on the Recipe identified by id.
 *      parameters:
 *        - name: id
 *          in: path
 *          description: Recipe Id.
 *          required: true
 *          type: integer
 *        - name: id
 *          in: body
 *          description: Group Id.
 *          required: true
 *          type: integer
 *      responses:
 *        200:
 *          description: An array of Groups for the Recipe.
 *          schema: 
 *            type: array
 *            items:
 *              $ref: '#/definitions/Group'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 *  /recipes/ingredients/{ingredientLineId}:
 *    delete:
 *      tags:
 *        - Recipes
 *      description: |
 *        Deletes one ingredient line identified by an id.
 *      parameters:
 *        - name: ingredientLineId
 *          in: query
 *          description: Id of the ingredient line to be deleted.
 *          required: true
 *          type: integer
 *      responses:
 *        200:
 *          description: Id of the deleted ingredient line.
 *          type: integer
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error' 
 */