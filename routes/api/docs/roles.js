/**
 * @swagger
 * definitions:
 *  Role:
 *    type: object
 *    properties:
 *      id:
 *        type: integer
 *        description: Role's id.
 *      role:
 *        type: string
 *        description: Roles's description.
 */