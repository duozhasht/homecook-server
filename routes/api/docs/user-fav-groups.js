/**
 * @swagger
 * definitions:
 *  UserFavGroup:
 *    type: object
 *    properties:
 *      groupId:
 *        type: integer
 *        description: Group's id.
 *      groupName:
 *        type: string
 *        description: Group's username.
 *      userId:
 *        type: integer
 *        description: User's id.
 */