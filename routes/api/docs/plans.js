/**  
 * @swagger
 * definitions:
 *   Plan:
 *     properties:
 *       id:
 *         description: Plan Id.
 *         type: integer
 *       title:
 *         description: Plan title.
 *         type: string
 *       description:
 *         description: Plan description.
 *         type: string
 *       minPrice:
 *         description: Plan minPrice.
 *         type: number
 *         format: double
 *       maxPrice:
 *         description: Plan maxPrice.
 *         type: number
 *         format: double
 *       planType:
 *         description: Plan Type.
 *         type: string
 *         enum: ['Full', 'Breakfast', 'Lunch', 'Dinner']
 *       provider:
 *         description: Plan provider.
 *         $ref: '#/definitions/User'
 * paths:
 *  /plans:
 *    get:
 *      tags:
 *        - Plans
 *      description: |
 *        Returns a collection of Plans. 
 *      parameters:
 *        - name: filter-owner
 *          in: query
 *          description: Identfies the filter to be applyed.
 *          required: false
 *          type: integer
 *        - name: filter-bought
 *          in: query
 *          description: Identfies the filter to be applyed.
 *          required: false
 *          type: integer
 *        - name: filter-provider-Id
 *          in: query
 *          description: Identfies the filter to be applyed and the value of the filter.
 *          required: false
 *          type: integer
 *      responses:
 *        200:
 *          description: The requested Plan.
 *          schema:
 *            $ref: '#/definitions/Plan'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 *    post:
 *      tags:
 *        - Plans
 *      description: |
 *        Creates a new Plan.
 *      parameters:
 *        - name: title
 *          in: body
 *          description: Plan title.
 *          required: true
 *          type: string
 *        - name: description
 *          in: body
 *          description: Plan description.
 *          required: true
 *          type: string
 *        - name: minPrice
 *          in: body
 *          description: Plan minPrice.
 *          required: true
 *          type: number
 *          format: double
 *        - name: maxPrice
 *          in: body
 *          description: Plan maxPrice.
 *          required: true
 *          type: number
 *          format: double
 *        - name: planType
 *          in: body
 *          description: Plan Type.
 *          required: true
 *          type: string
 *          enum: ['Full', 'Breakfast', 'Lunch', 'Dinner']
 *      responses:
 *        200:
 *          description: Returns the new Plan.
 *          schema:
 *            $ref: '#/definitions/Plan'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 *  /plans/{id}:
 *    get:
 *      tags:
 *        - Plans
 *      description: |
 *        Returns the Plan identified by id. 
 *      parameters:
 *        - name: id
 *          in: path
 *          description: Plan Id.
 *          required: true
 *          type: integer
 *      responses:
 *        200:
 *          description: The requested Plan.
 *          schema:
 *            $ref: '#/definitions/Plan'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 *    put:
 *      tags:
 *        - Plans
 *      description: |
 *        Update the Plan defined by id. 
 *      parameters:
 *        - name: id
 *          in: path
 *          description: Plan Id.
 *          required: true
 *          type: integer
 *        - name: title
 *          in: body
 *          description: Plan title.
 *          required: true
 *          type: string
 *        - name: description
 *          in: body
 *          description: Plan description.
 *          required: true
 *          type: string
 *        - name: minPrice
 *          in: body
 *          description: Plan title.
 *          required: true
 *          type: number
 *          format: double
 *        - name: maxPrice
 *          in: body
 *          description: Plan title.
 *          required: true
 *          type: number
 *          format: double
 *        - name: planType
 *          in: body
 *          description: Plan Type.
 *          required: true
 *          type: string
 *          enum: ['Full', 'Breakfast', 'Lunch', 'Dinner']
 *      responses:
 *        200:
 *          description: Returns the updated Plan.
 *          schema:
 *            $ref: '#/definitions/Plan'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 *    delete:
 *      tags:
 *        - Plans
 *      description: |
 *       Delete the plan identified by the id. 
 *      parameters:
 *        - name: id
 *          in: path
 *          description: Plan Id.
 *          required: true
 *          type: integer
 *      responses:
 *        200:
 *          description: The deleted Plan Id.
 *          schema:
 *            type: integer
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 *  /plans/{id}/meals:
 *    get:
 *      tags:
 *        - Plans
 *      description: |
 *        Returns the all the Meals of the Plan identified by id. 
 *      parameters:
 *        - name: id
 *          in: path
 *          description: Plan Id.
 *          required: true
 *          type: integer
 *      responses:
 *        200:
 *          description: An array of any number of Plan Meals.
 *          schema:
 *            type: array
 *            items:
 *              $ref: '#/definitions/PlanMeal'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 *    post:
 *      tags:
 *        - Plans
 *      description: |
 *        Add a Meal to the Plan defined by planId. 
 *      parameters:
 *        - name: planId
 *          in: body
 *          description: Plan Id.
 *          required: true
 *          type: integer
 *        - name: day
 *          in: body
 *          description: Meal day in the Plan.
 *          required: true
 *          type: integer
 *        - name: mealTime
 *          in: body
 *          description: Meal Time in the Plan.
 *          required: true
 *          schema:
 *            $ref: '#/definitions/MealTime'
 *        - name: recipeId
 *          in: body
 *          description: Recipe Id.
 *          required: true
 *          type: integer
 *      responses:
 *        200:
 *          description: Returns the updated Plan.
 *          schema:
 *            $ref: '#/definitions/Plan'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 *  /plans/{id}/meals/{planMealId}:
 *    delete:
 *      tags:
 *        - Plans
 *      description: |
 *       Delete the Meal from the Plan identified by the id. 
 *      parameters:
 *        - name: id
 *          in: path
 *          description: Plan Id.
 *          required: true
 *          type: integer
 *        - name: planMealId
 *          in: path
 *          description: Plan Meal Id.
 *          required: true
 *          type: integer
 *      responses:
 *        200:
 *          description: The deleted Meal Plan Id.
 *          schema:
 *            type: integer
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 * 
 */