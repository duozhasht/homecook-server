/**
 * @swagger
 * definitions:
 *   Group:
 *     properties:
 *       id:
 *         description: Group Id.
 *         type: integer
 *       name:
 *         description: Group description.
 *         type: string
 * paths:
 *  /groups/:
 *    get:
 *      tags:
 *        - Groups
 *      description: |
 *        Returns an array with all the Groups. 
 *      responses:
 *        200:
 *          description: An array with any number of Groups.
 *          schema:
 *            type: array
 *            items:
 *              $ref: '#/definitions/Group'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 *    post:
 *      tags:
 *        - Groups
 *      description: |
 *        Creates a new Group.
 *      parameters:
 *        - name: name
 *          in: body
 *          description: Group description.
 *          required: true
 *          type: string
 *      responses:
 *        200:
 *          description: An object with the created group.
 *          schema:
 *            $ref: '#/definitions/Group'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 *  /groups/{id}:
 *    get:
 *      tags:
 *        - Groups
 *      description: |
 *        Returns the requested Group. 
 *      parameters:
 *        - name: id
 *          in: path
 *          description: Group Id.
 *          required: true
 *          type: integer
 *      responses:
 *        200:
 *          description: An object with the selected Group.
 *          schema:
 *            $ref: '#/definitions/Group'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 *    put:
 *      tags:
 *        - Groups
 *      description: |
 *        Updates an existing Group.
 *      parameters:
 *        - name: id
 *          in: path
 *          description: Group Id.
 *          required: true
 *          type: integer
 *        - name: name
 *          in: body
 *          description: Group description.
 *          required: true
 *          type: string
 *      responses:
 *        200:
 *          description: An object with the updated Group.
 *          schema:
 *            $ref: '#/definitions/Group'
 *        default:
 *          description: Unexpected error
 *          schema:
 *            $ref: '#/definitions/Error'
 */