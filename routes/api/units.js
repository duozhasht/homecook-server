var express = require('express');
var router = express.Router();

var UnitModule = require('../../models/unit').unitModule;

/* /api/units */
router.get('/:id?', function(req, res, next) {
  if (!req.params.id) {
    UnitModule.findAll(
      function(err, result) {
        if (err)
          res.status(500).json({ message: err.msg, status: { code: err.code}});
        else
          res.status(200).json(result);
      }
    );
  }
  else {
    UnitModule.findById(req.params.id,
      function(err, result) {
        if (err)
          res.status(err.code).json({ message: err.msg, status: { code: err.code}});
        else
          res.status(200).json(result);
      }
    );
  }
});

router.post('/', function(req, res, next) {
  if (!req.body.name || !req.body.aggregate || !req.body.abbreviation) {
    res.status(400).json({ message: 'Bad Request', status: { code: 400} });
  }
  else {
    UnitModule.create(req.body.name, req.body.aggregate, req.body.abbreviation,
      function(err, result) {
        if (err) {
          res.status(err.code).json({ message: err.msg, status: { code: err.code}});
        }
        else {
          res.status(201).json(result);
        }
      }
    );
  }
})

router.put('/:id', function(req, res, next) {
  if (!req.body.name || req.body.aggregate === undefined || !req.body.abbreviation) {
    res.status(400).json({ message: 'Bad Request', status: { code: 400} });
  }
  else {
    UnitModule.update(req.params.id, req.body.name, req.body.aggregate, req.body.abbreviation,
      function(err, result) {
        if (err) {
          res.status(err.code).json({ message: err.msg, status: { code: err.code}});
        }
        else {
          res.status(201).json(result);
        }
      }
    );
  }
})

module.exports = router;
