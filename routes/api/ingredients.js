var express = require('express');
var router = express.Router();

var IngredientModule = require('../../models/ingredient').ingredientModule;

/* GET Ingredients page. */
/* /api/ingredients */
router.get('/:id?', function(req, res, next) {
  if (!req.params.id) {
    IngredientModule.findAll(function(err, result) {
      if (err) 
        res.status(err.code).json({ message: err.msg, status: { code: err.code}});
      else 
        res.status(200).json(result);
    })
  }
  else {
    IngredientModule.findByCategoryId(req.params.id, function(err, result) {
      if (err)
        res.status(err.code).json({ message: err.msg, status: { code: err.code}});
      else 
        res.status(200).json(result);
    })
  }
});

router.post('/', function(req, res, next) {
  if (!req.body.name || !req.body.categoryId) {
    res.status(500).json({ message: 'Bad Request', status: { code: 500 } });
  }
  else {
    IngredientModule.create(req.body.name, req.body.categoryId, function(err,result){
      if (err) 
        res.status(err.code).json({message: err.msg,status: { code: err.code} });
      else 
        res.status(201).json(result);
      }
    )
  }
})

router.put('/:id', function(req, res, next) {
  if (!req.body.name || !req.body.categoryId) {
    res.status(500).json({ message: 'Bad Request', status: { code: 500 } });
  }
  else {
    IngredientModule.update(req.params.id,req.body.name, req.body.categoryId, function(err,result){
      if (err) 
        res.status(err.code).json({message: err.msg, status: { code: err.code} });
      else 
        res.status(200).json(result);
      }
    )
  }
})



module.exports = router;
