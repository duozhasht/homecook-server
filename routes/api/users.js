var express = require('express');
var Promise = require('promise');
var router = express.Router();


var UserModule = require('../../models/user').userModule;
var SettingsModule = require('../../models/settings').settingsModule;
var UserFavGroupModule = require('../../models/user-fav-group').userFavGroupModule;


/* GET users listing. */
router.get('/', function(req, res, next) {
    UserModule.findAll(function(err, result) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.status(200).json(result);
        }
    });
});


router.post('/', function(req, res, next) {
    UserModule.create(req.body.username, req.body.email, req.body.password, req.body.servings, req.body.role.id, req.body.role.role, function(err, result) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.status(200).json(result);
        }
    });
});

router.get('/settings', function(req, res, next) {
    new Promise((resolve, reject) => {
            SettingsModule.findById(req.user.id, function(err, result) {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        })
        .then(res1 => {
            return UserFavGroupModule.findByUser(req.user.id, res1);
        })
        .then(res2 => {
            res.status(200).json(res2);
        })
        .catch(err => {
            res.status(500).send(err);
        });
});

router.put('/settings', function(req, res, next) {
    if (!req.body.username || !req.body.email || !req.body.servings || !req.body.groups) {
        res.status(400).json({ message: 'Bad Request', status: { code: 400 } });
        return;
    }
    new Promise((resolve, reject) => {
            SettingsModule.update(req.user.id, req.body.username, req.body.email, req.body.password, req.body.servings, function(err, result) {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        })
        .then(result => {
            return UserFavGroupModule.deleteSettings(req.user.id, req.body.groups, result);
        })
        .then(result => {
            return UserFavGroupModule.createSettings(req.user.id, req.body.groups, result);
        })
        .then(result => {
            return UserFavGroupModule.findByUser(req.user.id, result);
        })
        .then(result => {
            res.status(200).json(result);
        })
        .catch(err => {
            res.status(500).send(err);
        });
});

router.get('/settings/groups', function(req, res, next) {
    UserFavGroupModule.findByUser(req.user.id, {})
        .then(res1 => {
            res.status(200).json(res1.groups);
        })
        .catch(err => {
            res.status(500).send(err);
        });
});
router.post('/settings/groups', function(req, res, next) {
    if (!req.body.groupId) {
        res.status(400).json({ message: 'Bad Request', status: { code: 400 } });
        return;
    }
    UserFavGroupModule.create(req.user.id, req.body.groupId, function(err, result) {
        if (err) {
            if (!result) {
                res.status(500).send(err);
            } else {
                res.status(404).send(err);
            }
        } else {
            UserFavGroupModule.findByUser(req.user.id, {})
                .then(res1 => {
                    res.status(200).json(res1.groups);
                })
                .catch(err => {
                    res.status(500).send(err);
                });
        }
    });
});
router.delete('/settings/groups/:groupId', function(req, res, next) {
    UserFavGroupModule.delete(req.user.id, req.params.groupId, function(err, result) {
        if (err) {
            if (!result) {
                res.status(500).send(err);
            } else {
                res.status(404).send(err);
            }
        } else {
            res.status(200).json(result);
        }
    });
});

router.get('/:id/groups', function(req, res, next) {
    UserModule.findUserFavGroups(req.params.id /*req.user.id*/ , function(err, result) {
        if (err) {
            res.status(err.code).json(err);
        } else {
            res.status(200).json(result);
        }
    });
});

router.post('/:id/groups', function(req, res, next) {
    if (!req.body.groupId) {
        res.status(400).json({ message: 'Bad Request', status: { code: 400 } });
        return;
    }

    UserModule.addFavGroup(req.params.id, req.body.groupId, function(err, result) {
        if (err) {
            res.status(err.code).json(err);
        } else {
            res.status(200).json(result);
        }
    });
});


router.delete('/:id', function(req, res, next) {
    UserModule.delete(req.params.id, function(err, result) {
        if (err) {
            res.status(err.code).json(err);
        } else {
            res.status(200).json(result);
        }
    })
});

module.exports = router;