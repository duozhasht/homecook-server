var express = require('express');
var router = express.Router();
var MealModule = require('../../models/meal').mealModule;


router.get('/', function(req, res, next) {
  MealModule.findAll(req.user.id, req.header('filter-from'), req.header('filter-until'), function(err,result){
    if(err){
      res.status(err.code).json(err);
    }
    else{
      res.status(200).json(result);
    }
  });
});

router.post('/', function(req, res, next) {
  if (!req.body || !req.body.recipe || !req.body.mealTime || !req.body.servings) {
    res.status(400).json({ message: 'Bad Request', status: { code: 400}});
    return;
  }
  MealModule.create(req.user.id, req.body.recipe.id, new Date(req.body.date), req.body.mealTime, req.body.servings, function(err,result){
    if (err) {
      res.status(err.code).json(err);
    }
    else {
      res.status(200).json(result);
    }
  });
});

router.post('/add_plan_to_meals/:id', function(req, res, next) {
  var date = req.header('start-date');
  
  if (!req.params.id || !date) {
    res.status(400).json({ message: 'Bad Request', status: { code: 400}});
    return;
  }
  MealModule.addPlanToMeals(req.user.id, Number(req.params.id), date, function(err, result) {
    if (err) {
      res.status(err.code).json(err);
    }
    else {
      res.status(200).json(result);
    }
  });
});

router.put('/:id', function(req, res, next) {
  if (!req.body.recipe || !req.body.servings) {
    res.status(400).json({ message: 'Bad Request', status: { code: 400}});
    return;
  }
  MealModule.update(req.params.id, req.user.id, req.body.recipe.id, req.body.servings, function(err,result){
    if (err) {
      res.status(err.code).json(err);
    }
    else {
      res.status(200).json(result);
    }
  });
});

router.delete('/:id', function(req,res,next){
  MealModule.delete(req.params.id, function(err,result){
    if(err){
      res.status(err.code).json(err);
    }
    else {
      res.status(200).json(result);
    }
  })
})


module.exports = router;
