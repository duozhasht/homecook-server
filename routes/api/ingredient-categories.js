var express = require('express');
var router = express.Router();

/* GET ingredientCategories page. */
var IngredientCategoryModule = require('../../models/ingredient-category').ingredientCategoryModule;


/* GET users listing. */
router.get('/', function(req, res, next) {
  IngredientCategoryModule.findAll(function(err, result) {
    if (err) {
      res.status(err.code).send({ message: err.msg, status: { code: err.code}});
    }
    else {
      res.status(200).json(result);
    }
  });
});

router.post('/', function(req, res, next) {
  if(!req.body.name){
    res.status(400).json({ message: 'Bad Request.', status: { code: 400}});
    return ;
  }
  IngredientCategoryModule.create(req.body.name,function(err, result) {
    if (err) {
      res.status(err.code).send(err);
    }
    else {
      res.status(200).json(result);
    }
  });
});

router.put('/:id', function(req, res, next) {
  if(!req.body.name || !req.params.id){
    res.status(400).json({ message: 'Bad Request.', status: { code: 400}});
    return ;
  }
  IngredientCategoryModule.update(req.params.id,req.body.name,function(err, result) {
    if (err) {
      res.status(err.code).send(err);
    }
    else {
      res.status(200).json(result);
    }
  });
});

module.exports = router;
