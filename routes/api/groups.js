var express = require('express');
var router = express.Router();

var GroupModule = require('../../models/group').groupModule;


router.get('/:id?', function(req, res, next) {
  if (!req.params.id) {
    GroupModule.findAll(function(err, result) {
      if (err) 
        res.status(err.code).json({ message: err.msg, status: { code: err.code}});
      else 
        res.status(200).json(result);
    })
  }
  else {
    GroupModule.findById(req.params.id, function(err, result) {
      if (err)
        res.status(err.code).json({ message: err.msg, status: { code: err.code}});
      else 
        res.status(200).json(result);
    })
  }
});

router.post('/', function(req, res, next) {
  if (!req.body.name) {
    res.status(500).json({ message: 'Bad Request', status: { code: 500 } });
  }
  else {
    GroupModule.create(req.body.name, function(err,result){
      if (err) 
        res.status(err.code).json({message: err.msg,status: { code: err.code} });
      else 
        res.status(201).json(result);
      }
    )
  }
})

router.put('/:id', function(req, res, next) {
  if (!req.body.name) {
    res.status(500).json({ message: 'Bad Request', status: { code: 500 } });
  }
  else {
    GroupModule.update(req.params.id,req.body.name, function(err,result){
      if (err) 
        res.status(err.code).json({message: err.msg, status: { code: err.code} });
      else 
        res.status(200).json(result);
      }
    )
  }
})



module.exports = router;
