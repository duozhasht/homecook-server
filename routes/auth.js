var express = require('express');
var router = express.Router();

var jwt = require('jsonwebtoken');

var UserModule = require('../models/user').userModule;


router.post('/login', function(req, res, next) {
  //Check Bad Request
  if (!req.body || !req.body.email || !req.body.password) {
    res.status(400).json({ message: 'Bad Request', status: { code: 400} });
    return;
  }
  //Find User by Email
  UserModule.login(req.body.email, req.body.password, function(err, result) {
    if (err) {
      res.status(err.code).json(err);
      return;
    }

    var content = {
      id: result.id,
      username: result.username,
      email: result.email,
      role: result.role
    }

    var token = jwt.sign(content, 'fb09f1477be8601149f9a3715544bf35');
    res.status(200).json(token);
  })

});


router.post('/register', function(req, res, next) {
  //Check Bad Request
  if (!req.body || !req.body.email || !req.body.password) {
    res.status(400).json({ message: 'Bad Request', status: { code: 400} });
    return;
  }
  //Create User
  UserModule.create(req.body.email.match(/[\w|\d|\.]+/), req.body.email, req.body.password, 1, 3, null,function(err, result) {
    if (err) {
      res.status(err.code).json(err);
      return;
    }
    var token = jwt.sign(result, 'fb09f1477be8601149f9a3715544bf35');
    res.status(200).json(token);
  });
});




module.exports = router;
