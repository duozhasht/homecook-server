var db = require('./db');

var Ingredient = function(id, name, categoryId) {
  this.id = id;
  this.name = name;
  this.categoryId = categoryId;
}

var IngredientModule = function() {}

IngredientModule.prototype.create = function(name, categoryId, callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    //avoid SELECT * (objects shouldn't send all fields)
    connection.query('INSERT INTO Ingredient SET ?',
      {
        name: name,
        ingredientCategoryId: categoryId
      },
      function(err, result) {
        if (err) {
          err = { msg: err ,code: 500};
          callback(err, undefined);
        }
        else {
          var ingredient = new Ingredient(result.insertId, name, categoryId);
          callback(undefined, ingredient);
        }
    });
    connection.release();
  });
}

IngredientModule.prototype.update = function(id, name, categoryId, callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    //avoid SELECT * (objects shouldn't send all fields)
    connection.query(
      'UPDATE Ingredient SET '
        + 'name = "'+name+'", '
        + 'ingredientCategoryId = '+categoryId+' '
      + 'WHERE Ingredient.id = '+ id,
      function(err, result) {
        if (err) {
          err = { msg: err ,code: 500};
          callback(err, undefined);
        }
        else {
           if(result.affectedRows == 0){
             err = { msg: 'Ingredient not Found' ,code: 404};
             callback(err, undefined);
             return ;
           }
          var ingredient = new Ingredient(id, name, categoryId);
          callback(undefined, ingredient);
        }
    });
    connection.release();
  });
}

IngredientModule.prototype.findAll = function(callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    //avoid SELECT * (objects shouldn't send all fields)
    connection.query('SELECT * FROM Ingredient', function(err, rows, fields) {
      if (err) {
        err = { msg: err ,code: 500};
        callback(err, undefined);
      }
      else {
        var ingredientList = rows.map(function(row) {
          var ingredient = new Ingredient(row.id, row.name, row.ingredientCategoryId);
          return ingredient;
        })
        callback(undefined, ingredientList);
      }
    });
    connection.release();
  });
}

IngredientModule.prototype.findById = function(id, callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    //avoid SELECT * (objects shouldn't send all fields)
    connection.query('SELECT * FROM Ingredient where id = ?', id, function(err, rows, fields) {
      if (err) {
        err = { msg: err ,code: 500};
        callback(err, undefined);
        return;
      }
      if (rows.length == 0) {
        err = { msg: 'Ingredient Not Found' ,code: 404};
        callback(msg, undefined);
        return;
      }
      var ingredient = new Ingredient(rows[0].id, rows[0].name, rows[0].ingredientCategoryId);
      callback(undefined, ingredient);

    });
    connection.release();
  });
}

IngredientModule.prototype.findByCategoryId = function(categoryId, callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      callback('Datebase Connection Problem: '+ err, undefined);
      return;
    }
    //avoid SELECT * (objects shouldn't send all fields)
    connection.query('SELECT * FROM Ingredient where ingredientCategoryId = ?', categoryId, function(err, rows, fields) {
      if (err) {
        callback(err, undefined);
        return;
      }
      if (rows.length == 0) {
        callback('Ingredient Not Found', undefined);
        return;
      }
      var ingredientList = rows.map(function(row){
        var ingredient = new Ingredient(row.id, row.name, row.ingredientCategoryId);
        return ingredient;
      })
      
      callback(undefined, ingredientList);

    });
    connection.release();
  });
}

module.exports = {
  ingredient: Ingredient,
  ingredientModule: new IngredientModule()
}
