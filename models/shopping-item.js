var db = require('./db');

var Unit = require('./unit').unit;
var Ingredient = require('./ingredient').ingredient;

var ShoppingItem = function(id, date, recipeId, recipeServings, mealTime, mealServings, recipeQuantity, quantityChecked, ingredient, unit) {
  this.id = id;
  this.date = date;
  this.recipeId = recipeId;
  this.recipeServings = recipeServings
  this.mealTime = mealTime;
  this.mealServings = mealServings;
  this.recipeQuantity = recipeQuantity;
  this.quantityChecked = quantityChecked;
  this.ingredient = ingredient;
  this.unit = unit;
}

var ShoppingItemModule = function() {}

ShoppingItemModule.prototype.update = function(id, quantity ,callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    //avoid SELECT * (objects shouldn't send all fields)
    connection.query(
      'UPDATE ShoppingItemChecked SET ' 
      + 'ShoppingItemChecked.quantityChecked = ' + quantity 
        + ' WHERE ShoppingItemChecked.id = '+ id,
      function(err, rows, fields) {
        if (err) {
          err = { message: err , status: { code: 500 } };
          callback(err, undefined);
          return;
        }
        callback(undefined, id);
      }
    );
    connection.release();
  });
}

ShoppingItemModule.prototype.findAll = function(userId, from, until, callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    //avoid SELECT * (objects shouldn't send all fields)
    connection.query(
      'SELECT ' 
      + 'ShoppingItemView.*, ' 
      + 'Unit.name as "unitName", ' 
      + 'Unit.aggregate, ' 
      + 'Ingredient.name as "ingredientName", ' 
      + 'Ingredient.ingredientCategoryId ' 
        + 'FROM ShoppingItemView  ' 
          + 'INNER JOIN Unit ON ShoppingItemView.unitId = Unit.id ' 
          + 'INNER JOIN Ingredient ON ShoppingItemView.ingredientId = Ingredient.id '
        +'WHERE ShoppingItemView.date BETWEEN \'' + date_from.getFullYear()+"-"+(date_from.getMonth()+1)+"-"+date_from.getDate() + '\' AND \'' + date_until.getFullYear()+"-"+(date_until.getMonth()+1)+"-"+date_until.getDate() + '\'',
      function(err, rows, fields) {
        if (err) {
          err = { msg: err ,code: 500};
          callback(+ err, undefined);
          return;
        }
        var shoppingItemList = rows.map(function(row) {
          var shoppingItem = new ShoppingItem(
            row.id,
            row.date,
            row.recipeId,
            row.recipeServings,
            row.mealTime,
            row.mealServings,
            row.recipeQuantity,
            row.quantityChecked,
            new Ingredient(row.ingredientId, row.ingredientName, row.ingredientCategoryId),
            new Unit(row.unitId, row.unitName, row.aggregate == 1 ? true : false )
          );
          return shoppingItem;
        })
        callback(undefined, shoppingItemList);
      });
    connection.release();
  });
}


ShoppingItemModule.prototype.findByUserid = function(userId, from, until, callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }

    date_from = new Date(from);
    date_until =  new Date(until);

    //avoid SELECT * (objects shouldn't send all fields)
    connection.query(
      'SELECT ' 
      + 'ShoppingItemView.*, ' 
      + 'Unit.name as "unitName", ' 
      + 'Unit.aggregate, ' 
      + 'Unit.abbreviation, ' 
      + 'Ingredient.name as "ingredientName", ' 
      + 'Ingredient.ingredientCategoryId ' 
        + 'FROM ShoppingItemView  ' 
          + 'INNER JOIN Unit ON ShoppingItemView.unitId = Unit.id ' 
          + 'INNER JOIN Ingredient ON ShoppingItemView.ingredientId = Ingredient.id '
          + 'WHERE (ShoppingItemView.userId = ' + userId
          +') AND (ShoppingItemView.date BETWEEN \'' + date_from.getFullYear()+"-"+(date_from.getMonth()+1)+"-"+date_from.getDate() + '\' AND \'' + date_until.getFullYear()+"-"+(date_until.getMonth()+1)+"-"+date_until.getDate() + '\')',
      function(err, rows, fields) {
        if (err) {
          err = { msg: err ,code: 500};
          callback(+ err, undefined);
          return;
        }
        var shoppingItemList = rows.map(function(row) {
          var shoppingItem = new ShoppingItem(
            row.id,
            row.date,
            row.recipeId,
            row.recipeServings,
            row.mealTime,
            row.mealServings,
            row.recipeQuantity,
            row.quantityChecked,
            new Ingredient(row.ingredientId, row.ingredientName, row.ingredientCategoryId),
            new Unit(row.unitId, row.unitName, row.aggregate == 1 ? true : false, row.abbreviation)
          );
          return shoppingItem;
        })
        callback(undefined, shoppingItemList);
      });
    connection.release();
  });
}




module.exports = {
  shoppingItem: ShoppingItem,
  shoppingItemModule: new ShoppingItemModule()
}
