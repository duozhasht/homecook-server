var db = require('./db');

var Role = function(id, role) {
  this.id = id
  this.role = role
}

var RoleEnum = {
  free: 3,
  preemium: 4,
  stakeholder: 2,
  admin: 1
}

var RoleModule = function() {}

module.exports = {
  role: Role,
  roleEnum: RoleEnum,
  roleModule: new RoleModule()
}
