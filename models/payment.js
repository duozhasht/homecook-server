var db = require('./db');

var PaymentTypes = {
  Subscription: 'subscription',
  Plan: 'plan'
}

var PaymentPeriods = {
  Monthly: 'Mensal',
  Annually: 'Anual'
}

var Payment = function (id, userId, itemId, transactionId, payerId, payerEmail, addressCity, addressCountry, firstName, lastName, payerStatus,
  paymentDate, paymentType, paymentStatus, itemNumber, itemName,
  grossValue, fee, currency, discount, selectedOption) {
  this.id = id;
  this.userId = userId;
  this.itemId = itemId;
  this.transactionId = transactionId;
  this.payerId = payerId;
  this.payerEmail = payerEmail;
  this.addressCity = addressCity;
  this.addressCountry = addressCountry;
  this.firstName = firstName;
  this.lastName = lastName;
  this.payerStatus = payerStatus;
  this.paymentDate = paymentDate;
  this.paymentType = paymentType;
  this.paymentStatus = paymentStatus;
  this.itemNumber = itemNumber;
  this.itemName = itemName;
  this.grossValue = grossValue;
  this.fee = fee;
  this.currency = currency;
  this.discount = discount;
  this.selectedOption = selectedOption;
}

Payment.prototype.isSubscription = function() {
  return this.itemNumber == PaymentTypes.Subscription;
}

Payment.prototype.isMonthlySubscription = function() {
  return this.isSubscription() && this.selectedOption == PaymentPeriods.Monthly;
}

Payment.prototype.isAnnuallySubscription = function() {
  return this.isSubscription() && this.selectedOption == PaymentPeriods.Annually;
}

var PaymentModule = function () { }

PaymentModule.prototype.create = function (payment, callback) {
  db.getConnection(function (err, connection) {
    if (err) {
      err = { msg: err, code: 500 };
      callback(err, undefined)
    }
    connection.query('INSERT INTO Payment SET ?', payment,
      function (err, result) {
        if (err) {
          err = { msg: err, code: 500 };
          callback(err, undefined);
        }
        else {
          payment.id = result.insertId;
          callback(undefined, payment);
        }
      }
    );
    connection.release();
  })
}

var getFieldFromBody = function (body, field) {
  var value = body.match(field + '=.*\n');
  if (value) {
    value = value[0]
    value = value.replace(field + '=', '');
    value = value.replace(/\n/, '');
  }

  return value;
}

var createPaymentFromPayPal = function (body) {
  var paymentDate = getFieldFromBody(body, 'payment_date').replace(/\+/g, ' ');
  paymentDate = new Date(decodeURIComponent(paymentDate));

  return new Payment(
    null,
    null,
    Number(getFieldFromBody(body, 'custom')),
    getFieldFromBody(body, 'txn_id'),
    getFieldFromBody(body, 'payer_id'),
    decodeURIComponent(getFieldFromBody(body, 'payer_email')),
    getFieldFromBody(body, 'address_city'),
    getFieldFromBody(body, 'address_country'),
    getFieldFromBody(body, 'first_name').replace(/\+/g, ' '),
    getFieldFromBody(body, 'last_name').replace(/\+/g, ' '),
    getFieldFromBody(body, 'payer_status'),
    paymentDate,
    getFieldFromBody(body, 'payment_type'),
    getFieldFromBody(body, 'payment_status'),
    getFieldFromBody(body, 'item_number'),
    getFieldFromBody(body, 'item_name'),
    Number(getFieldFromBody(body, 'mc_gross')),
    Number(getFieldFromBody(body, 'mc_fee')),
    getFieldFromBody(body, 'mc_currency'),
    Number(getFieldFromBody(body, 'discount')),
    getFieldFromBody(body, 'option_selection1')
  );
}

module.exports = {
  payment: Payment,
  createPaymentFromPayPal: createPaymentFromPayPal,
  paymentTypes: PaymentTypes,
  paymentPeriods: PaymentPeriods,
  paymentModule: new PaymentModule()
};