var db = require('./db');

var Recipe = require('./recipe').recipe;


var Meal = function(id, date, mealTime, servings, recipe, userId) {
  this.id = id;
  this.date = date;
  this.mealTime = mealTime;
  this.servings = servings;
  this.recipe = recipe;
  this.userId = userId;
}

var MealModule = function() {}

MealModule.prototype.create = function(userId,recipeId,date,mealTime,servings,callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err,undefined);
      return;
    }
    connection.query('INSERT into Meal SET ?',
      { userId: userId,
        recipeId: recipeId, 
        date: date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate(), 
        mealTime: mealTime,
        servings: servings}, 
      function(err, result) {
        if (err) {
          err = { msg: err ,code: 500};
          callback(err,undefined);
          return;
        }
        new MealModule().findById(result.insertId,userId, function(err,result){
          if (err) {
            err = {
              msg: err,
              code: 500
            };
            callback(err, undefined);
          }
          else {
            callback(undefined, result);
          }
        });
        //var meal = new Meal(result.insertId, result.date, result.mealTime, result.servings, result.recipe);
        //callback(undefined, meal);
      });
      connection.release();
  });
}

MealModule.prototype.update = function(id, userId,recipeId,servings,callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err,undefined);
      return;
    }
    connection.query(
      'UPDATE Meal SET '
        + 'recipeId=' + recipeId + ', '
        + 'servings=' + servings + ' '
      + 'WHERE Meal.id ='+id,
      function(err, result) {
        if (err) {
          err = { msg: err ,code: 500};
          callback(err,undefined);
          return;
        }
        if(result.affectedRows == 0){
          err = { msg: 'Meal not Found' ,code: 404};
          callback(err, undefined);
          return ;
        }
        new MealModule().findById(id,userId, function(err,result){
          if (err) {
            err = {
              msg: err,
              code: 500
            };
            callback(err, undefined);
          }
          else {
            callback(undefined, result);
          }
        });
        //var meal = new Meal(result.insertId, result.date, result.mealTime, result.servings, result.recipe);
        //callback(undefined, meal);
      });
      connection.release();
  });
}

MealModule.prototype.findAll = function (userId, from, until, callback) {
  db.getConnection(function (err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    date_from = new Date(from);
    date_until =  new Date(until);

    connection.query('SELECT '
                          + 'Meal.id as "mealId", '
                          + 'Meal.date as "mealDate", '
                          + 'Meal.mealtime as "mealTime", '
                          + 'Meal.servings as "mealServings",  '
                          + 'Recipe.id as "recipeId", '
                          + 'Recipe.name as "recipeName", '
                          + 'Recipe.description as "recipeDescription", '
                          + 'Recipe.userId as "recipeUserId", '
                          + 'Recipe.difficulty as "recipeDifficulty", '
                          + 'Recipe.image as "recipeImage", '
                          + 'Recipe.servings as "recipeServings", '
                          + 'Recipe.price as "recipePrice",  '
                          + 'Recipe.time as "recipeTime", '
                          + 'Recipe.showDescription as "showDescription", '
                          + 'Recipe.isBreakfast as "isBreakfast", '
                          + 'IFNULL(UserFavRecipe.isFav, 0) AS isFav ' 
                        + 'FROM Meal '
                        + 'INNER JOIN Recipe '
                          + 'ON Meal.recipeId = Recipe.id ' 
                        + 'LEFT JOIN UserFavRecipe ' 
                          + 'ON Recipe.id = UserFavRecipe.recipeId ' 
                          + 'AND (UserFavRecipe.userId IS NULL OR UserFavRecipe.userId = ' + parseInt(userId) + ') '
                        + 'WHERE (Meal.userId = ' + userId
                          + ') AND (Meal.date BETWEEN \'' + date_from.getFullYear()+"-"+(date_from.getMonth()+1)+"-"+date_from.getDate() + '\' AND \'' + date_until.getFullYear()+"-"+(date_until.getMonth()+1)+"-"+date_until.getDate() + '\')'
                        , function(err, rows, fields){
      if (err) {
        err = { msg: err ,code: 500};
        callback(err,undefined);
        return;
      }
      var mealList = rows.map(function(row){
        var meal = new Meal( 
          row.mealId, 
          row.mealDate, 
          row.mealTime, 
          row.mealServings,
          new Recipe(
            row.recipeId,
            row.recipeName,
            row.recipeDescription,
            row.recipeImage,
            row.recipeServings,
            row.recipeRating,
            row.recipePrice,
            row.recipeTime,            
            row.recipeDifficulty,
            row.recipeUserId,
            row.isFav,
            null,
            null,
            row.showDescription,
            row.isBreakfast
        )); 
        return meal;
      });

      callback(err,mealList);
    });
    connection.release();
  });
}

MealModule.prototype.findById = function (id, userId, callback) {
  db.getConnection(function (err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    connection.query('SELECT '
                          + 'Meal.id as "mealId", '
                          + 'Meal.date as "mealDate", '
                          + 'Meal.mealtime as "mealTime", '
                          + 'Meal.servings as "mealServings",  '
                          + 'Recipe.id as "recipeId", '
                          + 'Recipe.name as "recipeName", '
                          + 'Recipe.description as "recipeDescription", '
                          + 'Recipe.difficulty as "recipeDifficulty", '
                          + 'Recipe.image as "recipeImage", '
                          + 'Recipe.servings as "recipeServings", '
                          + 'Recipe.price as "recipePrice",  '
                          + 'Recipe.time as "recipeTime" '
                        + 'FROM Meal '
                        + 'INNER JOIN Recipe '
                        + 'ON Meal.recipeId = Recipe.id '
                        + 'WHERE Meal.userId = ' + userId + ' AND Meal.id ='+id,
      function(err, rows, fields){
        if (err) {
          err = { msg: err ,code: 500};
          callback(err,undefined);
          return;
        }
          var meal = new Meal( 
            rows[0].mealId, 
            rows[0].mealDate, 
            rows[0].mealTime, 
            rows[0].mealServings,
            new Recipe(
              rows[0].recipeId,
              rows[0].recipeName,
              rows[0].recipeDescription,
              rows[0].recipeImage,
              rows[0].recipeServings,
              rows[0].recipeRating,
              rows[0].recipePrice,
              rows[0].recipeTime,
              rows[0].recipeDifficulty
            )
          );

        callback(err,meal);
      });
    connection.release();
  });
}

MealModule.prototype.delete = function(id, callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err, code: 500 };
      callback(err, undefined);
      return;
    }
    connection.query('DELETE FROM Meal WHERE id =' + id, function(err, result) {
      if (err) {
        err = { msg: err, code: 500 };
        callback(err, undefined);
        return;
      }
      callback(undefined, id);
    });
    connection.release();
  });
}

MealModule.prototype.addPlanToMeals = function(userId, planId, startDate, callback) {
  db.getConnection(function(err, connection) {
    if(err)  {
      err = { msg: err ,code: 500};
      callback(err, undefined);
    }
    
    connection.query('INSERT INTO Meal (userId, recipeId, date, mealTime, servings)'
      + ' SELECT ?, PlanMeal.recipeId, STR_TO_DATE("' + startDate + '", "%Y-%m-%d") + INTERVAL (PlanMeal.day - 1) DAY as date, PlanMeal.mealTime, servings.servings'
      + ' FROM (SELECT users.servings FROM (SELECT servings FROM User where id = ?) users) servings, PlanMeal'
      + ' WHERE planId = ?'
        + ' ON DUPLICATE KEY UPDATE ' 
          + ' Meal.recipeId = PlanMeal.recipeId,'
          + ' Meal.servings = servings.servings',
      [userId, userId, planId],
      function(err, result) {
        if(err) {
          err = { msg: err, code: 500 };
          callback(err, undefined);
        }
        else {
          callback(undefined, null);
        }
      }
    );
    connection.release();
  });
}

module.exports = {
  meal: Meal,
  mealModule: new MealModule()
};