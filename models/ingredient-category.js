var db = require('./db');

var IngredientCategory = function(id, name) {
  this.id = id;
  this.name = name;
}

var IngredientCategoryModule = function() {}

IngredientCategoryModule.prototype.create = function(name, callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    //avoid SELECT * (objects shouldn't send all fields)
    connection.query(
      'INSERT INTO IngredientCategory SET ?', 
      {
        name: name
      },
      function(err, result) {
        if (err) {
          err = { msg: err ,code: 500};
          callback(err, undefined);
        }
        else {
          var ingredientCategory = new IngredientCategory(result.insertId, name);
          callback(undefined, ingredientCategory);
        }
      }
    );
    connection.release();
  });
};

IngredientCategoryModule.prototype.update = function(id, name, callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    //avoid SELECT * (objects shouldn't send all fields)
    connection.query(
      'UPDATE IngredientCategory SET '
        +'name="' + name +'" '
      + 'WHERE IngredientCategory.id =' + id,
      function(err, result) {
        if (err) {
          err = { msg: err ,code: 500};
          callback(err, undefined);
        }
        else {
          if(result.affectedRows == 0){
            err = { msg: 'Not Found' ,code: 400};
            callback(err,undefined);
            return;
          }
          var ingredientCategory = new IngredientCategory(id, name);
          callback(undefined, ingredientCategory);
        }
      }
    );
    connection.release();
  });
};

IngredientCategoryModule.prototype.findAll = function(callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    //avoid SELECT * (objects shouldn't send all fields)
    connection.query('SELECT * FROM IngredientCategory ORDER BY id', function(err, rows, fields) {
      if (err) {
        err = { msg: err ,code: 500};
        callback(err, undefined);
      }
      else {
        var ingredientCategoryList = rows.map(function(row) {
          var ingredientCategory = new IngredientCategory(row.id, row.name);
          return ingredientCategory;
        })
        callback(undefined, ingredientCategoryList);
      }
    });
    connection.release();
  });
}

IngredientCategoryModule.prototype.findById = function(id, callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      callback('Datebase Connection Problem: '+ err, undefined);
      return;
    }
    //avoid SELECT * (objects shouldn't send all fields)
    connection.query('SELECT * FROM IngredientCategory where id = ?', id, function(err, rows, fields) {
      if (err) {
        callback(err, undefined);
        return;
      }
      if (rows.length == 0) {
        err = { msg: err ,code: 404};
        callback(err, undefined);
        return;
      }
      var ingredientCategory = new IngredientCategory(rows[0].id, rows[0].name);
      callback(undefined, ingredientCategory);

    });
    connection.release();
  });
}

module.exports = {
  ingredientCategory: IngredientCategory,
  ingredientCategoryModule: new IngredientCategoryModule()
}
