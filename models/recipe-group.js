var db = require('./db');

var RecipeGroup = function(groupId, groupName, recipeId) {
  this.groupId = groupId;
  this.groupName = groupName;
  this.recipeId = recipeId;
}

var RecipeGroupModule = function() {}

module.exports = {
  recipeGroup: RecipeGroup,
  recipeGroupModule: new RecipeGroupModule()
}