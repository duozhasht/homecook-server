var db = require('./db');
var Promise = require('promise');

var UserFavGroup = function(groupId, groupName, userId) {
    this.groupId = groupId;
    this.groupName = groupName;
    this.userId = userId;
}

var UserFavGroupModule = function() {}

UserFavGroupModule.prototype.findByUser = function(id, settings) {
    return new Promise((resolve, reject) => {
        db.getConnection(function(err, connection) {
            if (err) {
                err = { msg: err, code: 500 };
                reject(err);
            } else {
                //avoid SELECT * (objects shouldn't send all fields)
                connection.query('SELECT `Group`.id, `Group`.name FROM `Group` INNER JOIN `UserFavGroup` ON `Group`.id = `UserFavGroup`.groupId WHERE `UserFavGroup`.userId = ? ORDER BY `Group`.name ASC', id, function(err, rows, fields) {
                    if (err) {
                        err = { msg: err, code: 500 };
                        reject(err);
                    } else {
                        var groupList = rows.map(function(row) {
                            var group = new UserFavGroup(row.id, row.name, id);
                            return group;
                        })
                        settings.groups = groupList;
                        resolve(settings);
                    }
                });
                connection.release();
            }
        });
    });
}
UserFavGroupModule.prototype.findById = function(id, callback) {
    db.getConnection(function(err, connection) {
        if (err) {
            err = { msg: err, code: 500 };
            callback(err, undefined);
            return;
        }
        //avoid SELECT * (objects shouldn't send all fields)
        connection.query('SELECT `Group`.id, `Group`.name, `UserFavGroup`.userId FROM `Group` INNER JOIN `UserFavGroup` ON `Group`.id = `UserFavGroup`.groupId WHERE `UserFavGroup`.id = ?', id, function(err, rows, fields) {
            if (err) {
                err = { msg: err, code: 500 };
                callback(err, undefined);
                return;
            }
            var group = new UserFavGroup(rows[0].id, rows[0].name, rows[0].userId);
            callback(undefined, group);

        });
        connection.release();
    });
}
UserFavGroupModule.prototype.create = function(userId, groupId, callback) {
    db.getConnection(function(err, connection) {
        if (err) {
            err = { msg: err, code: 500 };
            callback(err, undefined);
            return;
        }
        connection.query('INSERT IGNORE INTO `UserFavGroup` SET ?', { userId: userId, groupId: groupId }, function(err, result) {
            if (err) {
                err = { msg: err, code: 500 };
                callback(err, undefined);
                return;
            } else {
                callback(undefined, result);
            }
            //new UserFavGroupModule().findById(result.insertId, callback);
        });
        connection.release();
    });
}
UserFavGroupModule.prototype.createSettings = function(userId, groups, settings) {
    return new Promise((resolve, reject) => {
        if (groups.length > 0) {
            var rows = groups.map(function(v, i, a) {
                var res = [];
                res.push(userId);
                res.push(v.groupId);
                return res;
            }, []);
            db.getConnection(function(err, connection) {
                if (err) {
                    err = { msg: err, code: 500 };
                    reject(err);
                }
                connection.query('INSERT IGNORE INTO `UserFavGroup` (userId, groupId) VALUES ?', [rows], function(err, result) {
                    if (err) {
                        err = { msg: err, code: 500 };
                        reject(err);
                    }
                    resolve(settings);
                });
                connection.release();
            });
        } else {
            resolve(settings);
        }
    });
}
UserFavGroupModule.prototype.delete = function(userId, groupId, callback) {
    db.getConnection(function(err, connection) {
        if (err) {
            err = { msg: err, code: 500 };
            callback(err, undefined);
            return;
        }
        connection.query('DELETE FROM `UserFavGroup` WHERE userId = ? AND groupId = ?', [userId, groupId], function(err, result) {
            if (err) {
                err = { msg: err, code: 500 };
                callback(err, undefined);
                return;
            }
            callback(undefined, new UserFavGroup(groupId, '', userId));
        });
        connection.release();
    });
}
UserFavGroupModule.prototype.deleteSettings = function(userId, groups, settings) {
    return new Promise((resolve, reject) => {
        var rows = groups.map(function(v, i, a) {
            return v.groupId;
        }, []);
        rows = rows.join(",");
        db.getConnection(function(err, connection) {
            if (err) {
                err = { msg: err, code: 500 };
                reject(err)
            }
            connection.query('DELETE FROM `UserFavGroup` WHERE userId = ?', [userId, rows], function(err, result) {
                if (err) {
                    err = { msg: err, code: 500 };
                    reject(err);
                } else {
                    resolve(settings);
                }
            });
            connection.release();
        });
    });
}

module.exports = {
    userFavGroup: UserFavGroup,
    userFavGroupModule: new UserFavGroupModule()
}