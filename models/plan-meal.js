var db = require('./db');
var Recipe = require('./recipe').recipe;

var PlanMeal = function(id, planId, day, mealTime, recipe) {
  this.id = id;
  this.planId = planId;
  this.day = day;
  this.mealTime = mealTime;
  this.recipe = recipe;
}

var PlanMealModule = function(){}

PlanMealModule.prototype.create = function(planId, day, mealTime, recipeId, callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    connection.query('INSERT into PlanMeal SET ?', {
      recipeId: recipeId,
      planId: planId,
      day: day,
      mealTime: mealTime,
    }, function(err, result) {
      if (err) {
        err = { msg: err ,code: 500};
        callback(err, undefined);
      }
      else {
        var planMeal = new PlanMeal(result.insertId, planId, day, mealTime, recipeId);
        callback(undefined, planMeal);
      }
    });
    connection.release();
  });
}


PlanMealModule.prototype.findByPlan = function(planId, userId, callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    //avoid SELECT * (objects shouldn't send all fields)
    connection.query(
      'SELECT '
        + 'PlanMeal.id AS planMealId, '
        + 'PlanMeal.recipeId AS recipeId, '
        + 'PlanMeal.planId AS planId, '
        + 'PlanMeal.mealTime AS mealTime, '
        + 'PlanMeal.day AS "day", '
        + 'Recipe.userId, '
        + 'Recipe.name, '
        + 'Recipe.description, '
        + 'Recipe.image, '
        + 'Recipe.servings, '
        + 'Recipe.price, '
        + 'Recipe.time, '
        + 'Recipe.difficulty, '
        + 'Recipe.active, '
        + 'Recipe.access, '
        + 'if(UserFavRecipe.isFav, true, false) AS "isFavorite", '
        + 'if(UserFavRecipe.rating, 0, UserFavRecipe.rating) AS "rating" ' 
        + 'FROM PlanMeal '
      + 'LEFT JOIN Recipe ON PlanMeal.recipeId = Recipe.id '
      + 'LEFT JOIN UserFavRecipe ON UserFavRecipe.recipeId = Recipe.id ' + 'AND (UserFavRecipe.id IS NULL OR UserFavRecipe.userId = '+parseInt(userId)+') '
      + 'WHERE PlanMeal.planId = ' + planId
      , function(err, rows, fields) {
      if (err) {
        err = { msg: err ,code: 500};
        callback(err, undefined);
        return;
      }
      if (rows.length == 0) {
        err = { msg: 'Plan Meals Not Found!' ,code: 404};
        callback(undefined, []);
        return;
      }
      var planMeals = rows.map(function(row){
        return new PlanMeal(row.planMealId, row.planId, row.day, row.mealTime,
          new Recipe(row.recipeId, row.name, row.description, row.image, row.servings, row.rating, row.price, row.time, row.difficulty, row.userId, row.isFavorite, row.access)
          );
      });

      callback(undefined, planMeals);
    });
    
    connection.release();
  });
}

PlanMealModule.prototype.delete = function(planMealId, callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err, code: 500};
      callback(err, undefined);
      return;
    }
    connection.query(
      'DELETE FROM PlanMeal '
      + 'WHERE PlanMeal.id = ' + parseInt(planMealId), 
      function(err, result) {
        if (err) {
          err = { msg: err, code: 500};
          callback(err, undefined);
        }
        else {
          if(result.affectedRows == 0){
             err = { msg: 'PlanMeal not Found' ,code: 404};
             callback(err, undefined);
           }
          else {
            callback(undefined, planMealId); 
          }
        }
      }
    );
    connection.release();
  });
}

module.exports = {
  planMeal: PlanMeal,
  planMealModule: new PlanMealModule()
}
