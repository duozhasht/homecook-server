var db = require('./db');

var Group = function(id, name) {
  this.id = id;
  this.name = name;
}

var GroupModule = function() {}

GroupModule.prototype.create = function(name, callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    //avoid SELECT * (objects shouldn't send all fields)
    connection.query('INSERT INTO `Group` SET ?',
      {
        name: name
      },
      function(err, result) {
        if (err) {
          err = { msg: err ,code: 500};
          callback(err, undefined);
        }
        else {
          var group = new Group(result.insertId, name);
          callback(undefined, group);
        }
    });
    connection.release();
  });
}

GroupModule.prototype.update = function(id, name, callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    //avoid SELECT * (objects shouldn't send all fields)
    connection.query(
      'UPDATE `Group` SET '
        + 'name = "'+name+'" '
      + 'WHERE Group.id = '+ id,
      function(err, result) {
        if (err) {
          err = { msg: err ,code: 500};
          callback(err, undefined);
        }
        else {
           if(result.affectedRows == 0){
             err = { msg: 'Group not Found' ,code: 404};
             callback(err, undefined);
             return ;
           }
          var group = new Group(id, name);
          callback(undefined, group);
        }
    });
    connection.release();
  });
}

GroupModule.prototype.findAll = function(callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    //avoid SELECT * (objects shouldn't send all fields)
    connection.query('SELECT * FROM `Group`', function(err, rows, fields) {
      if (err) {
        err = { msg: err ,code: 500};
        callback(err, undefined);
      }
      else {
        var groupList = rows.map(function(row) {
          var group = new Group(row.id, row.name);
          return group;
        })
        callback(undefined, groupList);
      }
    });
    connection.release();
  });
}

GroupModule.prototype.findById = function(id, callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    //avoid SELECT * (objects shouldn't send all fields)
    connection.query('SELECT * FROM `Group` where id = ?', id, function(err, rows, fields) {
      if (err) {
        err = { msg: err ,code: 500};
        callback(err, undefined);
        return;
      }
      if (rows.length == 0) {
        err = { msg: 'Group Not Found' ,code: 404};
        callback(msg, undefined);
        return;
      }
      var group = new Group(rows[0].id, rows[0].name);
      callback(undefined, group);

    });
    connection.release();
  });
}

GroupModule.prototype.findByRecipeId = function(id, callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    //avoid SELECT * (objects shouldn't send all fields)
    connection.query('SELECT `Group`.id, `Group`.name FROM `RecipeGroup` INNER JOIN `Group`ON `RecipeGroup`.groupId = `Group`.id WHERE recipeId = ?', id, function(err, rows, fields) {
      if (err) {
        err = { msg: err ,code: 500};
        callback(err, undefined);
        return;
      }
      var groupList = rows.map(function(row){
        var group = new Group(row.id, row.name);
        return group;
      });
      callback(undefined, groupList); 
    });
    connection.release();
  });
}

module.exports = {
  group: Group,
  groupModule: new GroupModule()
}
