var db = require('./db');
var Role = require('./role').role;
var User = require('./user').user;


var Provider = function(user, job, picture, description) {
  this.user = user;
  this.job = job; 
  this.picture = picture; 
  this.description = description;
}

var ProviderModule = function() {}


ProviderModule.prototype.findById = function(id, callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    //avoid SELECT * (objects shouldn't send all fields)
    connection.query(
      'SELECT User.*, UserRole.role '
      +'FROM User INNER JOIN UserRole ON User.userRoleId = UserRole.id '
        +'WHERE User.id = ' + id, function(err, rows, fields) {
      if (err) {
        err = { msg: err ,code: 500};
        callback(err, undefined);
        return;
      }
      if (rows.length == 0) {
        err = { msg: 'Provider Not Found!' ,code: 404};
        callback(err, []);
        return;
      }
      if(rows[0].role != 'stakeholder' ) {
        err = { msg: 'Not a Provider!' ,code: 404};
        callback(err, []);
        return;
      }
      var provider = new Provider(new User(rows[0].id, rows[0].username, rows[0].email, rows[0].password, rows[0].servings, new Role(rows[0].userRoleId, rows[0].role)),
                                  rows[0].job, rows[0].picture, rows[0].description);
      callback(undefined, provider);
    });
    connection.release();
  });
}

ProviderModule.prototype.findAll = function(callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    //avoid SELECT * (objects shouldn't send all fields)
    connection.query(
      'SELECT User.*, UserRole.role '
      +'FROM User INNER JOIN UserRole ON User.userRoleId = UserRole.id '
        +'WHERE UserRole.role = ' + '\'stakeholder\'', function(err, rows, fields) {
      if (err) {
        err = { msg: err ,code: 500};
        callback(err, undefined);
        return;
      }
      if (rows.length == 0) {
        err = { msg: 'Provider Not Found!' ,code: 404};
        callback(err, []);
        return;
      }
      var providers = rows.map(function(row){
        return new Provider(new User(row.id, row.username, row.email, row.password, row.servings, new Role(row.userRoleId, row.role)),
                                row.job, row.picture, row.description);
      })

      callback(undefined, providers);
    });
    connection.release();
  });
}

module.exports = {
  provider: Provider,
  providerModule: new ProviderModule()
}
