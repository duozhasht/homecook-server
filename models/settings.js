var db = require('./db');
var Role = require('./role').role;
var Role = require('./role').role;
var RoleEnum = require('./role').roleEnum;

var Settings = function(username, email, password, servings, role, groups) {
  this.username = username;
  this.email = email;
  this.password = password;
  this.servings = servings;
  this.role = role;
  this.groups = groups;
}

var SettingsModule = function() {}

SettingsModule.prototype.update = function(id, username, email, password, servings, callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    connection.query(
      'UPDATE User SET '
        + 'username = "'  + username + '", '
        + 'email = "'     + email    + '", '
        +  (password? 'password = "'  + password + '", ' : ' ')
        + 'servings = '   + servings + ' '
      + 'WHERE User.id = '+ id,
      (err, result) => {
        if (err) {
          err = { msg: err ,code: 500};
          callback(err, undefined);
        }
        else {
          SettingsModule.prototype.findById(id, (error, response) => {
            callback(undefined, response);
          });
        }
      });
    connection.release();
  });
}

SettingsModule.prototype.findById = function(id, callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    //avoid SELECT * (objects shouldn't send all fields)
    connection.query(
      'SELECT User.*, UserRole.role '
      +'FROM User INNER JOIN UserRole ON User.userRoleId = UserRole.id '
        +'WHERE User.id = ?', id, function(err, rows, fields) {
      if (err) {
        err = { msg: err ,code: 500};
        callback(err, undefined);
        return;
      }
      if (rows.length == 0) {
        err = { msg: 'User Not Found!' ,code: 404};
        callback(err, []);
        return;
      }
      var settings = new Settings(rows[0].username, rows[0].email, rows[0].password, rows[0].servings, new Role(rows[0].userRoleId, rows[0].role));
      callback(undefined, settings);
    });
    connection.release();
  });
}

SettingsModule.prototype.promoteToPreemium = function(id, callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    
    connection.query(
      'UPDATE User SET '
        + 'userRoleId = '   + RoleEnum.preemium + ' '
      + 'WHERE User.id = '+ id
      + ' AND User.userRoleId = '+ RoleEnum.free,
      function(err, result) {
        if (err) {
          err = { msg: err ,code: 500};
          callback(err, undefined);
        }
        else {
          callback(null, 'ok');
        }
      });
    connection.release();
  });
}


module.exports = {
  settings: Settings,
  settingsModule: new SettingsModule()
}
