var db = require('./db');

var Unit = function(id, name, aggregate, abbreviation) {
  this.id = id
  this.name = name
  this.aggregate = aggregate ? true : false
  this.abbreviation = abbreviation
}

var UnitModule = function() {}

UnitModule.prototype.create = function(name, aggregate, abbreviation, callback) {
  db.getConnection(function(err, connection) {
    if(err){
      err = { msg: err ,code: 500};
      callback(err,undefined)
    }
    connection.query('INSERT INTO Unit SET ?',
      { name: name,
        aggregate: aggregate,
        abbreviation: abbreviation
      },
      function(err,result) {
        if(err){
          err = { msg: err ,code: 500};
          callback(err,undefined);
        }
        else{
          var unit = new Unit(result.insertId, name, aggregate, abbreviation)
          callback(undefined, unit);
        }
      }
    );
    connection.release();
  })
}

UnitModule.prototype.update = function(id, name, aggregate, abbreviation, callback) {
  db.getConnection(function(err, connection) {
    if(err){
      err = { msg: err ,code: 500};
      callback(err,undefined);
    }
    connection.query(
      'UPDATE Unit SET '
        +'name = "' + name + '", '
        +'aggregate = ' + aggregate + ', '
        + 'abbreviation = "' + abbreviation + '" '
      +'WHERE Unit.id = ' + parseInt(id),function(err,result){
        if(err){
          err = { msg: err ,code: 500};
          callback(err,undefined);
        }
        else{
          if(result.affectedRows == 0){
            err = { msg: 'Not Found' ,code: 400};
            callback(err,undefined);
            return;
          }
          var unit = new Unit(id, name, aggregate, abbreviation)
          callback(undefined,unit);
        }
      }
    );
    connection.release();
  });
}


UnitModule.prototype.findAll = function(callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    connection.query('SELECT id, name, aggregate, abbreviation FROM Unit', function(err, rows, fields) {
      if (err) {
        err = { msg: err, code: 500};
        callback(err, undefined);
      }
      else {
        var unitList = rows.map(function(row) {
          var unit = new Unit(row.id, row.name, row.aggregate, row.abbreviation);
          return unit;
        })
        callback(undefined, unitList);
      }
    });
    connection.release();
  });
}

UnitModule.prototype.findById = function(id, callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err, code: 500};
      callback(err, undefined);
      return;
    }
    connection.query('SELECT id, name, aggregate, abbreviation FROM Unit where id = ?', id, function(err, rows, fields) {
      if (err) {
        err = { msg: err, code: 500};
        callback(err, undefined);
        return;
      }
      if (rows.length == 0) {
        err = { msg: 'Unit not found', code: 404}
        callback(err, undefined);
        return;
      }
      var unit = new Unit(rows[0].id, rows[0].name, rows[0].aggregate, rows[0].abbreviation);
      callback(undefined, unit);

    });
    connection.release();
  });
}

module.exports = {
  unit: Unit,
  unitModule: new UnitModule()
}
