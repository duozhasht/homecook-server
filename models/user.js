var fs = require('fs');

var db = require('./db');
var Role = require('./role').role;
var mailSender = require('./mail-sender');

var UserFavGroup = require('./user-fav-group').userFavGroup;


var User = function(id, username, email, password, servings, role) {
  this.id = id;
  this.username = username;
  this.email = email;
  this.password = password;
  this.servings = servings;
  this.role = role;
}

var UserModule = function() {}

UserModule.prototype.create = function(username, email, password, servings, roleId, role, callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    connection.query(
      'INSERT INTO User (username, email, password, servings, userRoleId) '
      +'VALUES ("'+username+'", "'+email+'", SHA2("'+ password +'",256), '+servings+', '+ roleId+') '
      ,function(err, result) {
      if (err) {
        err = { msg: err ,code: 500};
        callback(err, undefined);
      }
      else {
        new UserModule().findById(result.insertId, function(err, result){
          if(err){
            err = { msg: err ,code: 500};
            callback(err, undefined);
          }else{
            //(from, to, subject, text, html)
            emailBody = fs.readFileSync('./resources/after_Sign-in_email.html','utf8').toString().replace('\(\{\{EMAIL\}\}\)',email);
            //emailBody = emailBody.toString().replace('\(\{\{EMAIL\}\}\)',email);
            mailSender(email,'HomeCook',emailBody);
            callback(undefined, result);
          }
        });
      }
    });
    connection.release();
  });
}

UserModule.prototype.update = function(id, username, email, password, servings, roleId, role, callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    connection.query(
      'UPDATE User SET ?'
        + 'username = "'  + username + '", '
        + 'email = "'     + email    + '", '
        +  password? 'password = SHA2("'  + password + '", 256), ' : ' '
        + 'servings = '   + servings + ' '
      + 'WHERE User.id = '+ id,
      function(err, result) {
        if (err) {
          err = { msg: err ,code: 500};
          callback(err, undefined);
        }
        else {
          var user = new User(id, username, email, password, servings, new Role(roleId,role));
          callback(undefined, user);
        }
      });
    connection.release();
  });
}
        /*+ 'userRoleId = ' + roleId   + ' '*/


UserModule.prototype.findAll = function(callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    //avoid SELECT * (objects shouldn't send all fields)
    connection.query('SELECT User.*, UserRole.role FROM User INNER JOIN UserRole ON User.userRoleId = UserRole.id', function(err, rows, fields) {
      if (err) {
        err = { msg: err ,code: 500};
        callback(err, undefined);
        return;
      }
      var userList = rows.map(function(row) {
        var user = new User(row.id, row.username, row.email, row.password, row.servings, new Role(row.userRoleId, row.role));
        return user;
      })

      callback(undefined, userList);
    });
    connection.release();
  });
}

UserModule.prototype.findById = function(id, callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    //avoid SELECT * (objects shouldn't send all fields)
    connection.query(
      'SELECT User.*, UserRole.role '
      +'FROM User INNER JOIN UserRole ON User.userRoleId = UserRole.id '
        +'WHERE User.id = ?', id, function(err, rows, fields) {
      if (err) {
        err = { msg: err ,code: 500};
        callback(err, undefined);
        return;
      }
      if (rows.length == 0) {
        err = { msg: 'User Not Found!' ,code: 404};
        callback(err, []);
        return;
      }
      var user = new User(rows[0].id, rows[0].username, rows[0].email, rows[0].password, rows[0].servings, new Role(rows[0].userRoleId, rows[0].role));
      callback(undefined, user);
    });
    connection.release();
  });
}

UserModule.prototype.findByEmail = function(email, callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    //avoid SELECT * (objects shouldn't send all fields)
    connection.query(
      'SELECT User.*, UserRole.role '
      +'FROM User INNER JOIN UserRole ON User.userRoleId = UserRole.id '
      +'WHERE User.email = ?', email, function(err, rows, fields) {
      if (err) {
        err = { msg: err ,code: 500};
        callback(err, undefined);
        return;
      }
      if (rows.length == 0) {
        err = { msg: 'Email not found!' ,code: 404};
        callback(err, undefined);
        return;
      }
      var user = new User(rows[0].id, rows[0].username, rows[0].email, rows[0].password, rows[0].servings, new Role(rows[0].userRoleId, rows[0].role));
      callback(undefined, user);
    });
    connection.release();
  });
}

UserModule.prototype.login = function(email, password, callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    //avoid SELECT * (objects shouldn't send all fields)
    connection.query(
      'SELECT User.*, UserRole.role '
      +'FROM User INNER JOIN UserRole ON User.userRoleId = UserRole.id '
      +'WHERE User.email = "' + email + '" AND User.password = SHA2("' + password + '",256)', function(err, rows, fields) {
      if (err) {
        err = { msg: err ,code: 500};
        callback(err, undefined);
        return;
      }
      if (rows.length == 0) {
        err = { msg: 'No email match that password!' ,code: 400};
        callback(err, undefined);
        return;
      }
      var user = new User(rows[0].id, rows[0].username, rows[0].email, rows[0].password, rows[0].servings, new Role(rows[0].userRoleId, rows[0].role));
      callback(undefined, user);
    });
    connection.release();
  });
}

UserModule.prototype.findUserFavGroups = function(userId, callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    //avoid SELECT * (objects shouldn't send all fields)
    connection.query(
      'SELECT ' 
      + 'Group.id as "groupId", ' 
      + 'Group.name as "groupName", ' 
      + 'UserFavGroup.id ' 
        + 'FROM UserFavGroup ' 
          + 'INNER JOIN `Group` ON UserFavGroup.groupId = Group.id ' 
      + 'WHERE UserFavGroup.userId = ' + parseInt(userId),
      function(err, rows, fields) {
        if (err) {
          err = { msg: err ,code: 500};
          callback(err, undefined);
          return;
        }
        var userFavGroupList = rows.map(function(row) {
          var userFavGroup = new UserFavGroup(
            row.groupId,
            row.groupName,
            userId
          );
          return userFavGroup;
        });
        callback(undefined, userFavGroupList);
      });
    connection.release();
  });
}

UserModule.prototype.addFavGroup = function(userId, groupId, callback){
  db.getConnection(function(err,connection){
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    connection.query(
      'INSERT INTO UserFavGroup SET ?',
      {
        userId: parseInt(userId),
        groupId: parseInt(groupId)
      },
      function(err, result) {
        if (err) {
          err = { msg: err, code: 500};
          callback(err, undefined);
        }
        else {
          new UserModule().findUserFavGroups(userId, function(err, result) {
            if(err){
              err = { msg: err, code: 500};
              callback(err, undefined);
            }
            else {
              callback(undefined, result);
            }
          });
        }
      });
    connection.release();
  }); 
}


UserModule.prototype.delete = function(userId, callback
){
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err, code: 500};
      callback(err, undefined);
      return;
    }
    connection.query(
      'UPDATE User SET '
        + 'active = 0 '
			+ 'WHERE User.id = ' + parseInt(userId), 
      function(err, result) {
        if (err) {
          err = { msg: err, code: 500};
          callback(err, undefined);
        }
        else {
          if(result.affectedRows == 0){
             err = { msg: 'User not Found' ,code: 404};
             callback(err, undefined);
             return ;
           }
          var user = new User(userId);
          callback(undefined, user);
        }
      }
    );
    connection.release();
  });
}

module.exports = {
  user: User,
  userModule: new UserModule()
}
