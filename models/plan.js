var db = require('./db');
var Provider = require('./provider').provider;
var User = require('./user').user;
var Role = require('./role').role;

var Plan = function(id, title, description, minPrice, maxPrice, planType, provider, bought) {
    this.id = id;
    this.title = title;
    this.description = description;
    this.minPrice = minPrice;
    this.maxPrice = maxPrice;
    this.planType = planType;
    this.provider = provider;
    this.bought = bought;
}

var PlanModule = function() {}


PlanModule.prototype.create = function(userId, title, description, minPrice, maxPrice, planType, callback) {
    db.getConnection(function(err, connection) {
        if (err) {
            err = {
                msg: err,
                code: 500
            };
            callback(err, undefined);
            return;
        }
        connection.query('INSERT into Plan SET ?', {
            userId: userId,
            title: title,
            description: description,
            minPrice: minPrice,
            maxPrice: maxPrice,
            planType: planType
        }, function(err, result) {
            if (err) {
                err = {
                    msg: err,
                    code: 500
                };
                callback(err, undefined);
            } else {
                var plan = new Plan(result.insertId, title, description, minPrice, maxPrice, planType, null);
                callback(undefined, plan);
            }
        });
        connection.release();
    });
}

PlanModule.prototype.update = function(planId, userId, title, description, minPrice, maxPrice, planType, callback) {
    db.getConnection(function(err, connection) {
        if (err) {
            err = {
                msg: err,
                code: 500
            };
            callback(err, undefined);
            return;
        }
        connection.query('UPDATE Plan SET ' +
            'userId = ' + userId + ', ' +
            'title = "' + title + '", ' +
            'description = "' + description + '", ' +
            'minPrice = ' + minPrice + ', ' +
            'maxPrice = ' + maxPrice + ', ' +
            'planType = "' + planType + '" ' +
            'WHERE Plan.id = ' + planId,
            function(err, result) {
                if (err) {
                    err = {
                        msg: err,
                        code: 500
                    };
                    callback(err, undefined);
                } else {
                    var plan = new Plan(planId, title, description, minPrice, maxPrice, planType, null);
                    callback(undefined, plan);
                }
            });
        connection.release();
    });
}

PlanModule.prototype.findById = function(planId, userId, callback) {
    db.getConnection(function(err, connection) {
        if (err) {
            err = {
                msg: err,
                code: 500
            };
            callback(err, undefined);
            return;
        }
        //avoid SELECT * (objects shouldn't send all fields)
        connection.query(
            'SELECT ' +
            'Plan.id as planId, ' +
            'Plan.userId as planUserId, ' +
            'Plan.title as planTitle, ' +
            'Plan.description as planDescription, ' +
            'Plan.minPrice as planMinPrice, ' +
            'Plan.maxPrice as planMaxPrice, ' +
            'Plan.active as planActive, ' +
            'Plan.planType as planType, ' +
            'User.id as userId, ' +
            'User.userRoleId as userRoleId, ' +
            'User.username as username, ' +
            'User.picture as userPicture, ' +
            'User.job as userJob, ' +
            'User.description as userDescription, ' +
            '(SELECT count(*) FROM UserPlan WHERE UserPlan.planId = Plan.id AND UserPlan.userId = ' + userId + ') AS bought ' +
            'FROM Plan ' +
            'INNER JOIN User ON Plan.userId = User.id ' +
            'Where Plan.id = ' + planId + ' AND Plan.active = 1',
            function(err, rows, fields) {
                if (err) {
                    err = {
                        msg: err,
                        code: 500
                    };
                    callback(err, undefined);
                } else {
                    var plan = new Plan(rows[0].planId, rows[0].planTitle, rows[0].planDescription, rows[0].planMinPrice, rows[0].planMaxPrice, rows[0].planType,
                        new Provider(
                            new User(rows[0].userId, rows[0].username, 'not_avaliable', 'not_avaliable', 'not_avaliable', new Role(rows[0].userRoleId, 'stakeholder')),
                            rows[0].userJob, rows[0].userPicture, rows[0].userDescription),
                        Boolean(rows[0].bought)
                    );
                    callback(undefined, plan);
                }
            });
        connection.release();
    });
}


PlanModule.prototype.findByOwner = function(userId, limit, skip, callback) {
    db.getConnection(function(err, connection) {
        if (err) {
            err = {
                msg: err,
                code: 500
            };
            callback(err, undefined);
            return;
        }
        //avoid SELECT * (objects shouldn't send all fields)
        connection.query(
            'SELECT ' +
            'Plan.id as planId, ' +
            'Plan.userId as planUserId, ' +
            'Plan.title as planTitle, ' +
            'Plan.description as planDescription, ' +
            'Plan.minPrice as planMinPrice, ' +
            'Plan.maxPrice as planMaxPrice, ' +
            'Plan.active as planActive, ' +
            'Plan.planType as planType, ' +
            'User.id as userId, ' +
            'User.userRoleId as userRoleId, ' +
            'User.username as username, ' +
            'User.picture as userPicture, ' +
            'User.job as userJob, ' +
            'User.description as userDescription ' +
            'FROM Plan ' +
            'INNER JOIN User ON Plan.userId = User.id ' +
            'Where Plan.userId = ' + userId + ' AND Plan.active = 1 ' +
            'LIMIT ' + parseInt(limit) + ' ' +
            'OFFSET ' + parseInt(skip),
            function(err, rows, fields) {
                if (err) {
                    err = {
                        msg: err,
                        code: 500
                    };
                    callback(err, undefined);
                } else {
                    var planList = rows.map(function(row) {
                        var plan = new Plan(row.planId, row.planTitle, row.planDescription, row.planMinPrice, row.planMaxPrice, row.planType,
                            new Provider(
                                new User(row.userId, row.username, 'not_avaliable', 'not_avaliable', 'not_avaliable', new Role(row.userRoleId, 'stakeholder')),
                                rows.userJob, rows[0].userPicture, rows[0].userDescription),
                            row.bought
                        );
                        return plan;
                    })
                    callback(undefined, planList);
                }
            });
        connection.release();
    });
}

PlanModule.prototype.findByBought = function(userId, limit, skip, callback) {
    db.getConnection(function(err, connection) {
        if (err) {
            err = {
                msg: err,
                code: 500
            };
            callback(err, undefined);
            return;
        }
        //avoid SELECT * (objects shouldn't send all fields)
        connection.query(
            'SELECT ' +
            'Plan.id as planId, ' +
            'Plan.userId as planUserId, ' +
            'Plan.title as planTitle, ' +
            'Plan.description as planDescription, ' +
            'Plan.minPrice as planMinPrice, ' +
            'Plan.maxPrice as planMaxPrice, ' +
            'Plan.active as planActive, ' +
            'Plan.planType as planType, ' +
            'User.id as userId, ' +
            'User.userRoleId as userRoleId, ' +
            'User.username as username, ' +
            'User.picture as userPicture, ' +
            'User.job as userJob, ' +
            'User.description as userDescription ' +
            'FROM UserPlan ' +
            'INNER JOIN Plan ON UserPlan.planId = Plan.id ' +
            'INNER JOIN User ON Plan.userId = User.id ' +
            'Where UserPlan.userId = ' + userId + ' AND Plan.active = 1 ' +
            'LIMIT ' + parseInt(limit) + ' ' +
            'OFFSET ' + parseInt(skip),
            function(err, rows, fields) {
                if (err) {
                    err = {
                        msg: err,
                        code: 500
                    };
                    callback(err, undefined);
                } else {
                    var planList = rows.map(function(row) {
                        var plan = new Plan(row.planId, row.planTitle, row.planDescription, row.planMinPrice, row.planMaxPrice, row.planType,
                            new Provider(
                                new User(row.userId, row.username, 'not_avaliable', 'not_avaliable', 'not_avaliable', new Role(row.userRoleId, 'stakeholder')),
                                rows.userJob, rows[0].userPicture, rows[0].userDescription),
                            row.bought
                        );
                        return plan;
                    })
                    callback(undefined, planList);
                }
            });
        connection.release();
    });
}

PlanModule.prototype.findAll = function(userId, limit, skip, callback) {
    db.getConnection(function(err, connection) {
        if (err) {
            err = {
                msg: err,
                code: 500
            };
            callback(err, undefined);
            return;
        }
        //avoid SELECT * (objects shouldn't send all fields)
        connection.query(
            'SELECT ' +
            'Plan.id as planId, ' +
            'Plan.userId as planUserId, ' +
            'Plan.title as planTitle, ' +
            'Plan.description as planDescription, ' +
            'Plan.minPrice as planMinPrice, ' +
            'Plan.maxPrice as planMaxPrice, ' +
            'Plan.active as planActive, ' +
            'Plan.planType as planType, ' +
            'User.id as userId, ' +
            'User.userRoleId as userRoleId, ' +
            'User.username as username, ' +
            'User.picture as userPicture, ' +
            'User.job as userJob, ' +
            'User.description as userDescription ' +
            'FROM Plan ' +
            'INNER JOIN User ON Plan.userId = User.id ' +
            'LEFT JOIN UserPlan ON (Plan.id = UserPlan.planId OR Plan.id IS NULL) ' +
            'Where Plan.active = 1 AND (UserPlan.userId <> ' + userId + ' OR UserPlan.userId IS NULL) ' +
            'LIMIT ' + parseInt(limit) + ' ' +
            'OFFSET ' + parseInt(skip),
            function(err, rows, fields) {
                if (err) {
                    err = {
                        msg: err,
                        code: 500
                    };
                    callback(err, undefined);
                } else {
                    var planList = rows.map(function(row) {
                        var plan = new Plan(row.planId, row.planTitle, row.planDescription, row.planMinPrice, row.planMaxPrice, row.planType,
                            new Provider(
                                new User(row.userId, row.username, 'not_avaliable', 'not_avaliable', 'not_avaliable', new Role(row.userRoleId, 'stakeholder')),
                                rows.userJob, rows[0].userPicture, rows[0].userDescription)
                        );
                        return plan;
                    })
                    callback(undefined, planList);
                }
            });
        connection.release();
    });
}

PlanModule.prototype.delete = function(planId, callback) {
    db.getConnection(function(err, connection) {
        if (err) {
            err = {
                msg: err,
                code: 500
            };
            callback(err, undefined);
            return;
        }
        connection.query(
            'UPDATE Plan SET ' +
            'Plan.active = 0 ' +
            'WHERE Plan.id = ' + parseInt(planId),
            function(err, result) {
                if (err) {
                    err = {
                        msg: err,
                        code: 500
                    };
                    callback(err, undefined);
                } else {
                    if (result.affectedRows == 0) {
                        err = {
                            msg: 'Plan not Found',
                            code: 404
                        };
                        callback(err, undefined);
                    } else {
                        callback(undefined, planId);
                    }

                }
            }
        );
        connection.release();
    });
}

PlanModule.prototype.addPlanToUser = function(planId, userId, callback) {
    db.getConnection(function(err, connection) {
        if (err) {
            err = {
                msg: err,
                code: 500
            };
            callback(err, undefined)
        }
        connection.query('INSERT INTO UserPlan SET ?', {
                planId: planId,
                userId: userId
            },
            function(err, result) {
                if (err) {
                    err = {
                        msg: err,
                        code: 500
                    };
                    callback(err, undefined);
                } else {
                    var res = {
                        id: result.insertId,
                        planId: planId,
                        userId: userId
                    }

                    callback(undefined, res);
                }
            }
        );
        connection.release();
    })
}

module.exports = {
    plan: Plan,
    planModule: new PlanModule()
}