var db = require('./db');

var IngredientLine = function(id, quantity, unit, ingredient, recipeId) {
  this.id = id;
  this.quantity = quantity;
  this.unit = unit;
  this.ingredient = ingredient;
  this.recipeId = recipeId;
}

var IngredientLineModule = function() {}

module.exports = {
  ingredientLine: IngredientLine,
  ingredientLineModule: new IngredientLineModule()
}