var db = require('./db');

var Promise = require('promise');


var Ingredient = require('./ingredient').ingredient;
var IngredientLine = require('./ingredient-line').ingredientLine;
var RecipeGroup = require('./recipe-group').recipeGroup;
var Unit = require('./unit').unit;

var Recipe = function(id, name, description, image, servings, rating, price, time, difficulty, userId, isFavorite, access, groups, showDescription, isBreakfast) {
  this.id = id;
  this.name = name;
  this.description = description;
  this.image = image;
  this.servings = servings;
  this.rating = rating;
  this.price = price;
  this.time = time;
  this.userId = userId;
  this.difficulty = parseInt(difficulty);  
  this.isFavorite = isFavorite? true : false;
  this.access = access;
  this.groups = groups;
  this.showDescription = showDescription;
  this.isBreakfast = isBreakfast;
}

var RecipeModule = function() {}


RecipeModule.prototype.create = function(name, description, image, servings, price, time, difficulty, userId, access, showDescription, isBreakfast, callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err, code: 500};
      callback(err, undefined);
      return;
    }
    connection.query('INSERT INTO Recipe SET ? ', {
      userId: userId,
      name: name,
      description: description,
      image: image,
      servings: servings,
      price: price,
      time: time,
      difficulty: difficulty,
			access: access,
      showDescription: showDescription,
      isBreakfast: isBreakfast
    }, function(err, result) {
      if (err) {
        err = { msg: err, code: 500};
        callback(err, undefined);
      }
      else {
        var recipe = new Recipe(result.insertId, name, description, image, servings, null, price, time, difficulty, userId, false , access, new Object(), showDescription, isBreakfast);
        callback(undefined, recipe);
      }
    });
    connection.release();
  });
}

RecipeModule.prototype.update = function(id, name, description, image, servings, rating, price, time, difficulty, showDescription, isBreakfast, callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err, code: 500};
      callback(err, undefined);
      return;
    }
    connection.query(
      'UPDATE Recipe SET '
        + 'name="' + name + '", '
        + 'description="' + description + '", '
        + 'image="' + image + '", '
        + 'servings=' + servings + ', '
        + 'price=' + price + ', '
        + 'time="' + time + '", '
        + 'difficulty="' + difficulty + '", '
        + 'showDescription="' + showDescription + '", '
        + 'isBreakfast="' + isBreakfast + '" '
      + 'WHERE Recipe.id =' + id, 
      function(err, result) {
        if (err) {
          err = { msg: err, code: 500};
          callback(err, undefined);
        }
        else {
          if(result.affectedRows == 0){
             err = { msg: 'Recipe not Found' ,code: 404};
             callback(err, undefined);
             return ;
           }
          var recipe = new Recipe(id, name, description, image, servings, rating, price, time, difficulty, null, null, null, new Object(), showDescription, isBreakfast);
          callback(undefined, recipe);
        }
      }
    );
    connection.release();
  });
}

RecipeModule.prototype.addIngredientLine = function(recipeId, ingredientId, unitId, quantity, callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err, code: 500};
      callback(err, undefined);
      return;
    }
    connection.query(
      'INSERT INTO RecipeIngredient SET ?',
      {
        recipeId: recipeId,
        ingredientId: ingredientId,
        unitId: unitId,
        quantity: quantity
      }, 
      function(err, result) {
        if (err) {
          err = { msg: err, code: 500};
          callback(err, undefined);
        }
        else {
          var ingredientLine = new IngredientLine(
            result.insertId,
            quantity,
            new Unit(unitId),
            new Ingredient(ingredientId),
            recipeId
          );
          callback(undefined, ingredientLine);
        }
      });
    connection.release();
  });
}

RecipeModule.prototype.updateIngredientLine = function(recipeId, ingredientLineId, ingredientId, unitId, quantity, callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = {
        msg: err,
        code: 500
      };
      callback(err, undefined);
      return;
    }
    connection.query(
      'UPDATE RecipeIngredient SET ' +
      'ingredientId = ?, ' +
      'unitId = ?, ' +
      'quantity = ? ' +
      'WHERE id = ? '+
      'AND recipeId = ?', [
        ingredientId,
        unitId,
        quantity,
        ingredientLineId,
        recipeId
      ],
      function(err, result) {
        if (err) {
          err = {
            msg: err,
            code: 500
          };
          callback(err, undefined);
        } else {
          new RecipeModule().findRecipeIngredients(recipeId, function(err, result) {
            if (err) {
              err = {
                msg: err,
                code: 500
              };
              callback(err, undefined);
            } else {
              callback(undefined, result);
            }
          });
        }
      });
    connection.release();
  });
}

RecipeModule.prototype.deleteIngredientLine = function(ingredientLineId, callback){
   db.getConnection(function(err,connection){
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    connection.query('DELETE FROM RecipeIngredient WHERE id = ' + parseInt(ingredientLineId), function(err, result) {
      if (err) {
        err = { msg: err, code: 500 };
        callback(err, undefined);
        return;
      }
      callback(undefined, ingredientLineId);
    });
    connection.release();
  });
}

RecipeModule.prototype.findAll = function(userId,callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    connection.query(
      'SELECT '
        + 'Recipe.*, '
        + 'if(UserFavRecipe.isFav, true, false) AS "isFavorite", ',
        + 'if(UserFavRecipe.rating, 0, UserFavRecipe.rating) AS "rating" ' 
          + 'FROM Recipe '
            + 'LEFT JOIN UserFavRecipe '
              + 'ON UserFavRecipe.recipeId = Recipe.id '
                + 'AND (UserFavRecipe.id IS NULL OR UserFavRecipe.userId = '+parseInt(userId)+') ',
      function(err, rows, fields) {
        if (err) {
          err = { msg: err ,code: 500};
          callback(err, undefined);
        }
        else {
          var recipeList = rows.map(function(row){
              var recipe = new Recipe(row.id, row.name, row.description, row.image, row.servings, row.rating, row.price, row.time, row.difficulty, row.userId, row.isFavorite, row.access);
              return recipe;
          });
          callback(undefined, recipeList);



          /* Map Start to create Promises List */
        //   var recipePromisesList = rows.map(function(row){
        //     return new Promise(function(resolve,reject){
        //       new RecipeModule().findRecipeIngredients(row.id, function(err, result) {
        //         if (err) {
        //           //promise reject
        //           err = { msg: err ,code: 500};
        //           reject(err);
        //         }
        //         else {
        //           var recipe = new Recipe(row.id, row.name, row.description, row.image, row.servings, row.rating, row.price, row.time, row.difficulty, row.userId, row.isFavorite, result, row.access);
        //           //promise resolve
        //           resolve(recipe);
        //         }
        //       });
        //       /* Find Recipe Ingredients ended */
        //     });
        //     /* Promise Ended */
        //   });
        // /* Map Ended */
        //   Promise.all(recipePromisesList)
        //     .then(function(results){
        //       var recipeList = results;
        //       callback(undefined, recipeList);
        //     })
        //     .catch(function(reasons){
        //       callback(reasons, undefined);
        //     });
        }
        /* Else Ended */
    });
    /* Query Ended */
    connection.release();
  });
}

RecipeModule.prototype.findById = function(recipeId,userId,callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    connection.query(
      'SELECT DISTINCT ' + 
        'Recipe.id, ' + 
        'Recipe.userId, ' + 
        'Recipe.name, ' + 
        'Recipe.description, ' + 
        'Recipe.image, ' + 
        'Recipe.servings, ' + 
        'Recipe.price, ' + 
        'Recipe.time, ' + 
        'Recipe.difficulty, ' + 
        'Recipe.showDescription, ' + 
        'Recipe.isBreakfast, ' + 
        'IFNULL(UserFavRecipe.isFav, 0) AS isFav, ' + 
        'IFNULL(UserFavRecipe.rating, AverageRecipeRating.rating) AS rating ' + 
      'FROM Recipe ' + 
      'LEFT JOIN PlanMeal ' + 
        'ON Recipe.id = PlanMeal.recipeId ' + 
      'LEFT JOIN Plan ' + 
        'ON PlanMeal.PlanId = Plan.id ' + 
      'LEFT JOIN UserPlan ' + 
        'ON Plan.id = UserPlan.planId ' + 
      'LEFT JOIN UserFavRecipe ' + 
        'ON Recipe.id = UserFavRecipe.recipeId ' + 
        'AND (UserFavRecipe.userId IS NULL OR UserFavRecipe.userId = ' + parseInt(userId) + ') ' + 
      'LEFT JOIN AverageRecipeRating ' + 
        'ON Recipe.id = AverageRecipeRating.recipeId ' + 
      'WHERE ' + 
        'Recipe.active = 1 AND ' +
        'Recipe.id = '+ parseInt(recipeId) +
        ' AND ((Recipe.access = "public") ' + 
        'OR ' + 
        '((Recipe.access = "private" OR (Recipe.access = "restricted" OR UserPlan.userId = ' + parseInt(userId)+ '))))' ,
      function(err, rows, fields) {
        if (err) {
          err = { msg: err ,code: 500};
          callback(err, undefined);
        }
        else if(rows.length == 0){
            err = { msg:"Id not found", code: 404};
            callback(err, undefined);
          }
        else {

          var recipe = new Recipe(rows[0].id, rows[0].name, rows[0].description, rows[0].image, rows[0].servings, rows[0].rating, rows[0].price, rows[0].time, rows[0].difficulty, rows[0].userId,  rows[0].isFav, null, new Object(), rows[0].showDescription, rows[0].isBreakfast);
          callback(undefined, recipe);

        //   /* Map Start to create Promises List */
        //   var recipePromisesList = rows.map(function(row){
        //     return new Promise(function(resolve,reject){
        //       new RecipeModule().findRecipeIngredients(row.id, function(err, result) {
        //         if (err) {
        //           //promise reject;
        //           err = { msg: err ,code: 500};
        //           reject(err);
        //         }
        //         else {
        //           var recipe = new Recipe(
        //             row.id, 
        //             row.name, 
        //             row.description, 
        //             row.image, 
        //             row.servings, 
        //             row.rating, 
        //             row.price, 
        //             row.time, 
        //             row.difficulty, 
        //             row.userId,
        //             row.isFavorite, 
        //             result,
        //             row.active,
        //             row.access
        //           );
        //           //promise resolve;
        //           resolve(recipe);
        //         }
        //       });
        //       /* Find Recipe Ingredients ended */
        //     });
        //     /* Promise Ended */
        //   });
        // /* Map Ended */
        //   Promise.all(recipePromisesList)
        //     .then(function(results){
        //         var recipeList = results;
              
        //       callback(undefined, recipeList);
        //     })
        //     .catch(function(reasons){
        //       callback(reasons, undefined);
        //     });
        }

        /* Else Ended */
    });
    /* Query Ended */
    connection.release();
  });
}

RecipeModule.prototype.findByName = function(userId, search, isFavorite, sortByRating, sortByPrice, sortByTime, offset, limit, groups, isBreakfast, callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    var groupIdFilter = '';
    var isBreakfastFilter = '';

    var tokenizer = groups.split(",");
    if (tokenizer[0] != '' && tokenizer[0] != null){
      groupIdFilter = groupIdFilter + ' AND Recipe.id in (SELECT RecipeGroup.recipeId FROM RecipeGroup WHERE RecipeGroup.groupID = ' + tokenizer[0]; 
    }
      
    for (index = 1; index < tokenizer.length; ++index) {
      if (tokenizer[index] != '' && tokenizer[index] != null){
        groupIdFilter = groupIdFilter + ' OR RecipeGroup.groupId = ' + tokenizer[index];
      }
    }
      
     if (tokenizer[0] != '' && tokenizer[0] != null) {
      groupIdFilter = groupIdFilter + ') ';
    }
    if (isBreakfast != '') {
      isBreakfastFilter = ' AND Recipe.isBreakfast = ' + isBreakfast + ' ';
    }
    connection.query(
      'SELECT DISTINCT ' + 
        'Recipe.id, ' + 
        'Recipe.userId, ' + 
        'Recipe.name, ' + 
        'Recipe.description, ' + 
        'Recipe.image, ' + 
        'Recipe.servings, ' + 
        'Recipe.price, ' + 
        'Recipe.time, ' + 
        'Recipe.difficulty, ' + 
        'Recipe.showDescription, ' + 
        'Recipe.isBreakfast, ' + 
        'IFNULL(UserFavRecipe.isFav, 0) AS isFav, ' + 
        'IFNULL(UserFavRecipe.rating, AverageRecipeRating.rating) AS rating ' + 
      'FROM Recipe ' + 
      'LEFT JOIN PlanMeal ' + 
        'ON Recipe.id = PlanMeal.recipeId ' + 
      'LEFT JOIN Plan ' + 
        'ON PlanMeal.PlanId = Plan.id ' + 
      'LEFT JOIN UserPlan ' + 
        'ON Plan.id = UserPlan.planId ' + 
      'LEFT JOIN UserFavRecipe ' + 
        'ON Recipe.id = UserFavRecipe.recipeId ' + 
        'AND (UserFavRecipe.userId IS NULL OR UserFavRecipe.userId = ' + parseInt(userId) + ') ' + 
      'LEFT JOIN AverageRecipeRating ' + 
        'ON Recipe.id = AverageRecipeRating.recipeId ' + 
      'WHERE ' + 
        'Recipe.name LIKE "%' + search + '%" AND ' +
        (isFavorite ? 'UserFavRecipe.isFav = 1 AND ' : '') +
        'Recipe.active = 1 AND ' +
        '((Recipe.access = "public") ' + 
        'OR ' + 
        '((Recipe.access = "private" OR Recipe.access = "restricted") AND UserPlan.userId = ' + parseInt(userId) + ')) ' + 
        isBreakfastFilter +
        groupIdFilter +
      'ORDER BY ' +
        (sortByRating ? 'rating ' + sortByRating + ', ' : '') +
        (sortByPrice? 'Recipe.price ' + sortByPrice + ', ' : '') +
        (sortByTime? 'Recipe.time ' + sortByTime + ', ' : '') +
        'Recipe.name ASC' +
      ' LIMIT ' + parseInt(limit) + 
      ' OFFSET ' + parseInt(offset) ,
      function(err, rows, fields) {
        if (err) {
          err = { msg: err ,code: 500};
          callback(err, undefined);
          return;
        }
        else {
          var recipeList = rows.map(function(row){
              var recipe = new Recipe(row.id, row.name, row.description, row.image, row.servings, row.rating, row.price, row.time, row.difficulty, row.userId, row.isFav, row.access, new Object(), row.showDescription, row.isBreakfast);
              return recipe;
          });
          callback(undefined, recipeList);

        //   /* Map Start to create Promises List */
        //   var recipePromisesList = rows.map(function(row){
        //     return new Promise(function(resolve,reject){
        //       new RecipeModule().findRecipeIngredients(row.id, function(err, result) {
        //         if (err) {
        //           //promise reject
        //           err = { msg: err ,code: 500};
        //           reject(err);
        //         }
        //         else {
        //           var recipe = new Recipe(row.id, row.name, row.description, row.image, row.servings, row.rating, row.price, row.time, row.difficulty, row.userId, row.isFav, result);
        //           //promise resolve
        //           resolve(recipe);
        //         }
        //       });
        //       /* Find Recipe Ingredients ended */
        //     });
        //     /* Promise Ended */
        //   });
        // /* Map Ended */
        //   Promise.all(recipePromisesList)
        //     .then(function(results){
        //       var recipeList = results;
        //       callback(undefined, recipeList);
        //     })
        //     .catch(function(reasons){
        //       callback(reasons, undefined);
        //     });
        }
        /* Else Ended */
    });
    /* Query Ended */
    connection.release();
  });
}

RecipeModule.prototype.findSuggestions = function(userId,callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    connection.query(
      'SELECT DISTINCT ' + 
        'Recipe.id, ' + 
        'Recipe.userId, ' + 
        'Recipe.name, ' + 
        'Recipe.description, ' + 
        'Recipe.image, ' + 
        'Recipe.servings, ' + 
        'Recipe.price, ' + 
        'Recipe.time, ' + 
        'Recipe.difficulty, ' +  
        'Recipe.showDescription, ' + 
        'Recipe.isBreakfast, ' + 
        'IFNULL(UserFavRecipe.isFav, 0) AS isFav, ' + 
        'IFNULL(UserFavRecipe.rating, AverageRecipeRating.rating) AS rating ' + 
      'FROM Recipe ' + 
      'LEFT JOIN PlanMeal ' + 
        'ON Recipe.id = PlanMeal.recipeId ' + 
      'LEFT JOIN Plan ' + 
        'ON PlanMeal.PlanId = Plan.id ' + 
      'LEFT JOIN UserPlan ' + 
        'ON Plan.id = UserPlan.planId ' + 
      'LEFT JOIN UserFavRecipe ' + 
        'ON Recipe.id = UserFavRecipe.recipeId ' + 
        'AND (UserFavRecipe.userId IS NULL OR UserFavRecipe.userId = ' + parseInt(userId) + ') ' + 
      'LEFT JOIN AverageRecipeRating ' + 
        'ON Recipe.id = AverageRecipeRating.recipeId ' + 
      'LEFT JOIN RecipeGroup ' + 
        'ON Recipe.id = RecipeGroup.recipeId ' +
      'WHERE ' + 
        'Recipe.active = 1 ' +
        ' AND ((Recipe.access = "public") ' + 
        'OR ' + 
        '((Recipe.access = "private" OR (Recipe.access = "restricted" OR UserPlan.userId = ' + parseInt(userId) + '))))' +
        ' AND Recipe.id IN ' +
          '(SELECT ' +
            'RecipeGroup.recipeId ' + 
          'FROM UserFavGroup ' + 
          'LEFT JOIN RecipeGroup ' +
            'ON UserFavGroup.groupId = RecipeGroup.groupId ' +
          'WHERE ' +
            'UserFavGroup.userId = ' + parseInt(userId) + ') ' + 
            'AND RecipeGroup.groupId IN (SELECT groupId FROM UserFavGroup WHERE UserFavGroup.userId = ' + parseInt(userId) + ') ' +
        'ORDER BY RAND() ' + 
        'LIMIT 5',
      function(err, rows, fields) {
        if (err) {
          err = { msg: err ,code: 500};
          callback(err, undefined);
        }
        else {
          var recipeList = rows.map(function(row) {
            var recipe = new Recipe(row.id, row.name, row.description, row.image, row.servings, row.rating, row.price, row.time, row.difficulty, row.userId,  row.isFavorite,null, new Object(), row.showDescription, row.isBreakfast);
            return recipe;
          });
          callback(undefined, recipeList);
        }
    });
    connection.release();
  });
}

RecipeModule.prototype.findRecipeIngredients = function(recipeId, callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    //avoid SELECT * (objects shouldn't send all fields)
    connection.query(
      'SELECT ' 
      + 'Ingredient.id as "ingredientId", ' 
      + 'Ingredient.name as "ingredientName", ' 
      + 'Ingredient.ingredientCategoryId as "categoryId", ' 
      + 'RecipeIngredient.quantity, '
      + 'RecipeIngredient.id, ' 
      + 'Unit.id as "unitId", ' 
      + 'Unit.name as "unitName" ' 
        + 'FROM RecipeIngredient ' 
          + 'INNER JOIN Ingredient ON RecipeIngredient.ingredientId = Ingredient.id ' 
          + 'INNER JOIN Unit ON RecipeIngredient.unitId = Unit.id ' 
      + 'WHERE RecipeIngredient.recipeId = ?', parseInt(recipeId),
      function(err, rows, fields) {
        if (err) {
          err = { msg: err ,code: 500};
          callback(err, undefined);
          return;
        }
        var ingredientLineList = rows.map(function(row) {
          var ingredientLine = new IngredientLine(
            row.id,
            row.quantity,
            new Unit(row.unitId, row.unitName),
            new Ingredient(row.ingredientId, row.ingredientName),
            recipeId
          );
          return ingredientLine;
        })
        callback(undefined, ingredientLineList);
      });
    connection.release();
  });
}

RecipeModule.prototype.addGroups = function(recipeId, groups, result) {
    return new Promise((resolve, reject) => {
        if (groups.length > 0) {
            var rows = groups.map(function(v) {
                var res = [];
                res.push(recipeId);
                res.push(v.groupId);
                return res;
            }, []);
            db.getConnection(function(err, connection) {
                if (err) {
                    err = { msg: err, code: 500 };
                    reject(err);
                }
                connection.query('INSERT INTO `RecipeGroup` (recipeId, groupId) VALUES ?', [rows], function(err, result) {
                    if (err) {
                        err = { msg: err, code: 500 };
                        reject(err);
                    }
                    resolve(result);
                });
                connection.release();
            });
        } else {
          resolve(result);
        }
      });
}

RecipeModule.prototype.findRecipeGroups = function(recipeId, callback) {
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    //avoid SELECT * (objects shouldn't send all fields)
    connection.query(
      'SELECT ' 
      + 'Group.id as "groupId", ' 
      + 'Group.name as "groupName", ' 
      + 'RecipeGroup.id ' 
        + 'FROM RecipeGroup ' 
          + 'INNER JOIN `Group` ON RecipeGroup.groupId = Group.id ' 
      + 'WHERE RecipeGroup.recipeId = ' + parseInt(recipeId),
      function(err, rows, fields) {
        if (err) {
          err = { msg: err ,code: 500};
          callback(err, undefined);
          return;
        }
        var recipeGroupList = rows.map(function(row) {
          var recipeGroup = new RecipeGroup(
            row.groupId,
            row.groupName,
            recipeId
          );
          return recipeGroup;
        });
        callback(undefined, recipeGroupList);
      });
    connection.release();
  });
}

RecipeModule.prototype.deleteGroups = function(recipeId, groups, callback) {  
  db.getConnection(function(err, connection) {
      if (err) {
          err = { msg: err, code: 500 };
          reject(err)
      }
      connection.query('DELETE FROM `RecipeGroup` WHERE recipeId = ?', [recipeId], function(err, result) {
          if (err) {
              err = { msg: err, code: 500 };
              callback(err, undefined);
          } else {
              callback(undefined, result);
          }
      });
      connection.release();
  });
}

RecipeModule.prototype.findUserRecepies = function( user1Id,user2Id,search, sortByRating, sortByPrice, sortByTime, offset, limit, groups, isBreakfast, callback ){
	db.getConnection(function(err, connection){
		if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
		}

    var groupIdFilter = '';
    var isBreakfastFilter = '';

    var tokenizer = groups.split(",");
    if (tokenizer[0] != '' && tokenizer[0] != null){
      groupIdFilter = groupIdFilter + ' AND Recipe.id in (SELECT RecipeGroup.recipeId FROM RecipeGroup WHERE RecipeGroup.groupID = ' + tokenizer[0]; 
    }
      
    for (index = 1; index < tokenizer.length; ++index) {
      if (tokenizer[index] != '' && tokenizer[index] != null){
        groupIdFilter = groupIdFilter + ' OR RecipeGroup.groupId = ' + tokenizer[index];
      }
    }
      
    if (tokenizer[0] != '' && tokenizer[0] != null) {
      groupIdFilter = groupIdFilter + ') ';
    }

    if (isBreakfast != '') {
      isBreakfastFilter = ' AND Recipe.isBreakfast = ' + isBreakfast + ' ';
    }


		connection.query(
      'SELECT DISTINCT ' + 
        'Recipe.id, ' + 
        'Recipe.userId, ' + 
        'Recipe.name, ' + 
        'Recipe.description, ' + 
        'Recipe.image, ' + 
        'Recipe.servings, ' + 
        'Recipe.price, ' + 
        'Recipe.time, ' + 
        'Recipe.difficulty, ' + 
        'Recipe.showDescription, ' + 
        'Recipe.isBreakfast, ' +
        'IFNULL(UserFavRecipe.isFav, 0) AS isFav, ' + 
        'IFNULL(UserFavRecipe.rating, AverageRecipeRating.rating) AS rating ' + 
      'FROM Recipe ' + 
      // 'LEFT JOIN PlanMeal ' + 
      //   'ON Recipe.id = PlanMeal.recipeId ' + 
      // 'LEFT JOIN Plan ' + 
      //   'ON PlanMeal.PlanId = Plan.id ' + 
      // 'LEFT JOIN UserPlan ' + 
      //   'ON Plan.id = UserPlan.planId ' + 
      'LEFT JOIN UserFavRecipe ' + 
        'ON Recipe.id = UserFavRecipe.recipeId ' + 
        'AND (UserFavRecipe.userId IS NULL OR UserFavRecipe.userId = ' + parseInt(user1Id) + ') ' + 
      'LEFT JOIN AverageRecipeRating ' + 
        'ON Recipe.id = AverageRecipeRating.recipeId ' + 
      'WHERE ' + 
        'Recipe.name LIKE "%' + search + '%" AND ' +
        'Recipe.active = 1 AND ' +
        'Recipe.userId = ' + parseInt(user2Id) +
        // '((Recipe.access = "public") ' + 
        // 'OR ' + 
        // '((Recipe.access = "private" OR Recipe.access = "restricted") AND UserPlan.userId = ' + parseInt(user1Id) + ')) ' + 
        isBreakfastFilter +
        groupIdFilter + 
      ' ORDER BY ' +
        (sortByRating ? 'rating ' + sortByRating + ', ' : '') +
        (sortByPrice? 'Recipe.price ' + sortByPrice + ', ' : '') +
        (sortByTime? 'Recipe.time ' + sortByTime + ', ' : '') +
        'Recipe.name ASC' +
      ' LIMIT ' + parseInt(limit) + 
      ' OFFSET ' + parseInt(offset),
			function(err, rows, fields) {
        if (err) {
          err = { msg: err ,code: 500};
          callback(err, undefined);
        }
        else {
          var recipeList = rows.map(function(row){
              var recipe = new Recipe(row.id, row.name, row.description, row.image, row.servings, row.rating, row.price, row.time, row.difficulty, row.userId, row.isFavorite, row.access, new Object(), row.showDescription, row.isBreakfast);
              return recipe;
          });
          callback(undefined, recipeList);

        //   /* Map Start to create Promises List */
        //   var recipePromisesList = rows.map(function(row){
        //     return new Promise(function(resolve,reject){
        //       new RecipeModule().findRecipeIngredients(row.id, function(err, result) {
        //         if (err) {
        //           //promise reject;
        //           err = { msg: err ,code: 500};
        //           reject(err);
        //         }
        //         else {
        //           var recipe = new Recipe(
        //             row.id, 
        //             row.name, 
        //             row.description, 
        //             row.image, 
        //             row.servings, 
        //             row.rating, 
        //             row.price, 
        //             row.time, 
        //             row.difficulty, 
        //             row.userId,
        //             row.isFavorite, 
        //             result,
        //             row.active,
        //             row.access
        //           );
        //           //promise resolve;
        //           resolve(recipe);
        //         }
        //       });
        //       /* Find Recipe Ingredients ended */
        //     });
        //     /* Promise Ended */
        //   });
        // /* Map Ended */
        //   Promise.all(recipePromisesList)
        //     .then(function(results){
        //         var recipeList = results;
              
        //       callback(undefined, recipeList);
        //     })
        //     .catch(function(reasons){
        //       callback(reasons, undefined);
        //     });
        }
        /* Else Ended */
    });
    /* Query Ended */
    connection.release();
	});
}

RecipeModule.prototype.delete = function(recipeId, callback){
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err, code: 500};
      callback(err, undefined);
      return;
    }
    connection.query(
      'UPDATE Recipe SET '
        + 'active = 0 '
			+ 'WHERE Recipe.id = ' + parseInt(recipeId), 
      function(err, result) {
        if (err) {
          err = { msg: err, code: 500};
          callback(err, undefined);
        }
        else {
          if(result.affectedRows == 0){
             err = { msg: 'Recipe not Found' ,code: 404};
             callback(err, undefined);
             return ;
           }
          var recipe = new Recipe(recipeId);
          callback(undefined, recipe);
        }
      }
    );
    connection.release();
  });
}

/*
RecipeModule.prototype.rate = function(userId, recipeId, rating, isFavorite){
  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    //avoid SELECT * (objects shouldn't send all fields)
    connection.query(
      'INSERT INTO UserFavRecipe (userId, recipeId, isFav, rating) VALUES (' +
        + 'UserFavRecipe.userId = ' + userId + ', '
        + 'UserFavRecipe.recipeId = ' + recipeId + ', '
        + 'UserFavRecipe.isFav = ' + isFavorite + ', '
        + 'UserFavRecipe.rating = ' + rating + ''
        + ') '
      + 'ON DUPLICATE KEY UPDATE ' 
        + 'UserFavRecipe.isFav = ' + isFavorite + ', '
        + 'UserFavRecipe.rating = ' + rating + ''
      ,
      function(err, rows, fields) {
        if (err) {
          err = { msg: err ,code: 500};
          callback(err, undefined);
          return;
        }
     
}
*/

RecipeModule.prototype.favorite = function(userId, recipeId, isFavorite, callback) {
  isFavorite = isFavorite ? 1 : 0;

  db.getConnection(function(err, connection) {
    if (err) {
      err = { msg: err ,code: 500};
      callback(err, undefined);
      return;
    }
    
    connection.query(
      'INSERT INTO UserFavRecipe (userId, recipeId, isFav) VALUES (' +
        + userId + ', '
        + parseInt(recipeId) + ', '
        + isFavorite + ') '
      + 'ON DUPLICATE KEY UPDATE ' 
        + 'UserFavRecipe.isFav = ' + isFavorite
      ,
      function(err, result) {
        if (err) {
          err = { msg: err ,code: 500};
          callback(err, undefined);
          return;
        }
        else {
          callback(undefined, {});
        }
      }
    );
    connection.release();
  });
}

module.exports = {
  recipe: Recipe,
  recipeModule: new RecipeModule()
}
